package com.mtime;

import java.util.Calendar;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.mtime.data.Showtime;
import com.mtime.util.MtimeUtils;

public class MovieSessionDetailActivity extends AbstractMtimeActivity {
	private String movieName, cinemaName;
	TextView showtimeTime, showtimeDetail, showDate;
	List<Showtime> list;
	int showtimeId, movieId;
	int pos;
	String[] weekChinese;

	Button nextSessionBtn, prevSessionBtn;

	/** Called when the activity is first created. */
	@SuppressWarnings("unchecked")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.act_movie_session_detail);
		list = (List<Showtime>) getIntent().getExtras().getSerializable(
				Constants.KEY_SHOWTIME_LIST);
		movieName = getIntent().getExtras().getString(Constants.KEY_MOVIE_NAME);
		cinemaName = getIntent().getExtras().getString(
				Constants.KEY_CINEMA_NAME);
		pos = getIntent().getExtras()
				.getInt(Constants.KEY_POS_IN_SHOWTIME_LIST);

		movieId = getIntent().getExtras().getInt(Constants.KEY_MOVIE_ID);

		Showtime showtime = list.get(pos);

		showtimeId = showtime.getId();

		((TextView) findViewById(R.id.tv_movieName)).setText(movieName);
		((TextView) findViewById(R.id.tv_CinemaName)).setText(cinemaName);

		showtimeTime = (TextView) findViewById(R.id.tv_ShowtimeTime);
		showtimeDetail = (TextView) findViewById(R.id.tv_ShowtimeDetail);
		showDate = (TextView) findViewById(R.id.tv_ShowDate);

		nextSessionBtn = (Button) findViewById(R.id.btn_next_session);
		prevSessionBtn = (Button) findViewById(R.id.btn_prev_session);

		weekChinese = new String[] { "", getString(R.string.sun), getString(R.string.mon),
				getString(R.string.tues), getString(R.string.wen),
				getString(R.string.thur), getString(R.string.fri),
				getString(R.string.sat) };

		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(showtime.getTime());

		showDate.setText(MtimeUtils.getSimpleDate(showtime.getTime()) + " "
				+ weekChinese[c.get(Calendar.DAY_OF_WEEK)]);
		checkButton();
		showtimeTime.setText(MtimeUtils.getTime(showtime.getTime()));
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	private void showShowtime(Showtime showtime) {
		showtimeTime.setText(MtimeUtils.getTime(showtime.getTime()));
		StringBuffer detail = new StringBuffer("  ");
		detail.append(showtime.getVersion());
		if (showtime.getDuration() > 0)
			detail.append("(" + showtime.getDuration()
					+ getString(R.string.minutes) + ") ");
		if (showtime.getPrice() != null && showtime.getPrice().length() > 0)
			detail.append(getString(R.string.cinema_price)
					+ showtime.getPrice());
		detail.append(" " + showtime.getHall());

		showtimeDetail.setText(detail);

	}

	private void checkButton() {
		if (pos == 0) {
			prevSessionBtn.setEnabled(false);
		}
		if (pos == list.size() - 1) {
			nextSessionBtn.setEnabled(false);
		}

		if (pos > 0) {
			prevSessionBtn.setEnabled(true);
		}
		if (pos < list.size() - 1) {
			nextSessionBtn.setEnabled(true);
		}

	}

	@Override
	void doOperation() {
		final Button nextSessionBtn = (Button) findViewById(R.id.btn_next_session);
		final Button prevSessionBtn = (Button) findViewById(R.id.btn_prev_session);

		OnClickListener lsner = new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (v.getId() == R.id.btn_next_session) {
					pos++;
				} else if (v.getId() == R.id.btn_prev_session) {
					pos--;
				}
				if (pos < 0)
					pos = 0;
				if (pos >= list.size())
					pos = list.size() - 1;

				checkButton();
				Showtime showtime = list.get(pos);
				showtimeTime.setText(MtimeUtils.getTime(showtime.getTime()));
				showtimeDetail.setText("Loading...");

				final int showtimeId = showtime.getId();

				new Task() {
					Showtime showtime;

					@Override
					public void after() {
						if (null == showtime)
							MtimeUtils
									.gotoErrorActivity(MovieSessionDetailActivity.this);
						showShowtime(showtime);
					}

					@Override
					public void before() throws Exception {
						showtime = rs.getShowtimeDetail(showtimeId);
					}
				}.start();

			}

		};

		nextSessionBtn.setOnClickListener(lsner);
		prevSessionBtn.setOnClickListener(lsner);

		((Button) findViewById(R.id.btn_select_this_session))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent intent = new Intent();
						intent.setClass(MovieSessionDetailActivity.this,
								MovieSessionSelectedActivity.class);
						intent.putExtra(Constants.KEY_MOVIE_ID, movieId);
						intent.putExtra(Constants.KEY_MOVIE_NAME, movieName);
						intent.putExtra(Constants.KEY_SHOWTIME_ID, list
								.get(pos).getId());
						intent.putExtra(Constants.KEY_CINEMA_NAME, cinemaName);
						startActivity(intent);
					}
				});

		new Task() {
			Showtime showtime;

			@Override
			public void after() {
				if (null == showtime)
					MtimeUtils
							.gotoErrorActivity(MovieSessionDetailActivity.this);
				else
					showShowtime(showtime);
			}

			@Override
			public void before() throws Exception {
				showtime = rs.getShowtimeDetail(showtimeId);
			}
		}.start();
	}
}
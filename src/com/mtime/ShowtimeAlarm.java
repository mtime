package com.mtime;

import java.util.List;

import org.apache.http.cookie.Cookie;

import com.mtime.util.MtimeUtils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

/**
 * When there's an alarm, show the notification in the status bar. When user
 * tries to click it, just start Mtime.
 * 
 * @author Dreamcast
 */
public class ShowtimeAlarm extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        // If user logged, show the notification.
        List<Cookie> loginCookies = MtimeUtils.getCookies();
        if (null != loginCookies
                && 0 != loginCookies.size()
                && MtimeUtils.getNotifySetting(context,
                        Constants.KEY_SETTING_RATING_NOTIFY)) {
            // Cancel it in alarm manager.
            int alarmNumber = intent.getExtras().getInt(
                    Constants.KEY_ALARM_NUMBER);
            MtimeUtils.cancelEventInBroadcast(context, ShowtimeAlarm.class, alarmNumber);

            String movieDetail = intent.getStringExtra(Constants.MOVIE_DETAIL);
            int movieId = intent.getIntExtra(Constants.KEY_MOVIE_ID, 0);

            Log.d("Alarm", "RatingReminder:"
                    + (R.string.rating_reminder + movieId) + "," + movieId);

            String movieName = intent.getExtras().getString(
                    Constants.KEY_MOVIE_NAME);

            Toast.makeText(context, movieDetail, Toast.LENGTH_LONG).show();
            // look up the notification manager service
            NotificationManager notification = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            // The details of our fake message
            String from = context.getString(R.string.app_name);
            from = movieName;
            CharSequence message = intent.getStringExtra(Constants.KEY_MESSAGE);

            // The PendingIntent to launch our activity if the user selects this
            // notification

            Intent goIntent = new Intent(context, RatingActivity.class);
            goIntent.putExtra(Constants.KEY_MOVIE_ID, movieId);
            goIntent.setData(Uri.parse(Constants.MTIME_ALARM + alarmNumber));
            
            PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                    goIntent, 0);

            // construct the Notification object.
            Notification notif = new Notification(R.drawable.stat_sample,
                    message, System.currentTimeMillis());

            // Set the info for the views that show in the notification panel.
            notif.setLatestEventInfo(context, from, message, contentIntent);

            // after a 100ms delay, vibrate for 250ms, pause for 100 ms and
            // then vibrate for 500ms.
            notif.vibrate = new long[] { 100, 250, 100, 500 };

            // Note that we use R.layout.incoming_message_panel as the ID for
            // the notification. It could be any integer you want, but we use
            // the convention of using a resource id for a string related to
            // the notification. It will always be a unique number within your
            // application.

            notification.notify(R.string.rating_reminder + movieId, notif);
        }
    }
}

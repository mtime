package com.mtime;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mtime.data.Cinema;
import com.mtime.util.MtimeUtils;

public class CinemaDetailActivity extends AbstractMtimeActivity {

	private int cinemaId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.act_cinema_detail);

		Intent intent = getIntent();
		if (intent == null)
			throw new NullPointerException("no intent");

		cinemaId = ((Integer) intent
				.getSerializableExtra(Constants.KEY_CINEMA_ID)).intValue();
	}

	private void setUpCinemaDetailView(final Cinema cinema) {
		// Cinema name
		TextView textView = (TextView) findViewById(R.id.cinemaName);
		textView.setText(cinema.getName());

		// Cinema scores
		textView = (TextView) findViewById(R.id.cinemaOverallScore);
		String[] r = MtimeUtils.formatRatingScore(cinema.getRatingScore());
		textView.setText(r[0] + r[1]);

		textView = (TextView) findViewById(R.id.cinemaComfortScore);
		r = MtimeUtils.formatRatingScore(cinema.getRatingScore4Comfort());
		textView.setText(r[0] + r[1]);

		textView = (TextView) findViewById(R.id.cinemaSoundScore);
		r = MtimeUtils.formatRatingScore(cinema.getRatingScore4Sound());
		textView.setText(r[0] + r[1]);

		textView = (TextView) findViewById(R.id.cinemaServiceScore);
		r = MtimeUtils.formatRatingScore(cinema.getRatingScore4Service());
		textView.setText(r[0] + r[1]);

		textView = (TextView) findViewById(R.id.cinemaEquipmentScore);
		r = MtimeUtils.formatRatingScore(cinema.getRatingScore4Equipment());
		textView.setText(r[0] + r[1]);

		textView = (TextView) findViewById(R.id.cinemaTrafficScore);
		r = MtimeUtils.formatRatingScore(cinema.getRatingScore4Traffic());
		textView.setText(r[0] + r[1]);

		textView = (TextView) findViewById(R.id.cinemaArroundScore);
		r = MtimeUtils.formatRatingScore(cinema.getRatingScore4Arround());
		textView.setText(r[0] + r[1]);

		textView = (TextView) findViewById(R.id.cinemaArroundScore);
		r = MtimeUtils.formatRatingScore(cinema.getRatingScore4Arround());
		textView.setText(r[0] + r[1]);

		textView = (TextView) findViewById(R.id.cinemaAddress);
		textView.setText(cinema.getAddress());

		textView = (TextView) findViewById(R.id.cinemaTel);
		textView.setText(cinema.getTel());

		textView = (TextView) findViewById(R.id.cinemaRoute);
		textView.setText(cinema.getRoute());

		final double latitude = cinema.getGeoLatitude();
		final double longitude = cinema.getGeoLongitude();

		TextView mapLink = (TextView) findViewById(R.id.mapLink);
		mapLink.setLinksClickable(true);
		mapLink.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent();

				ArrayList<MapPoi> poiList = new ArrayList<MapPoi>();
				poiList.add(new MapPoi.Builder(latitude, longitude,
						R.drawable.my_location_marker_green, true)
						.setPopupWindowTitle(cinema.getName())
						.setPopupWindowSnippet(cinema.getAddress()).build());
				i.putExtra(Constants.KEY_MAP_POI, poiList);
				i.setClass(CinemaDetailActivity.this,
						CinemaLocationActivity.class);
				CinemaDetailActivity.this.startActivity(i);
			}
		});
		if (0 == latitude || 0 == longitude) {
			mapLink.setVisibility(View.GONE);
		}

		// Cinema facilities
		Map<String, Integer> fmap = getCinemaFacilities(cinema);
		String[] fnames = fmap.keySet().toArray(new String[] {});

		LinearLayout fTable = (LinearLayout) findViewById(R.id.cinemaFacilities);

		if (fmap.size() == 0) {
			fTable.setVisibility(View.GONE);
		}
		for (int i = 0; i < fnames.length; i += 2) {
			View row = getLayoutInflater().inflate(
					R.layout.item_cinema_facilities, null);

			ImageView image = (ImageView) row.findViewById(R.id.cinema_fimage1);
			TextView text = (TextView) row.findViewById(R.id.cinema_ftext1);
			ImageView image2 = (ImageView) row
					.findViewById(R.id.cinema_fimage2);
			TextView text2 = (TextView) row.findViewById(R.id.cinema_ftext2);

			image.setImageResource(fmap.get(fnames[i]).intValue());
			text.setText(fnames[i]);
			if ((i + 1) < fnames.length) {
				image2.setImageResource(fmap.get(fnames[i + 1]).intValue());
				text2.setText(fnames[i + 1]);
			} else {
				image2.setVisibility(View.INVISIBLE);
				text2.setVisibility(View.INVISIBLE);
			}

			fTable.addView(row);
			if ((i + 2) < fnames.length) {
				fTable.addView(getLayoutInflater().inflate(
						R.layout.item_cinema_facilities_divider, null));
			}
		}
	}

	private Map<String, Integer> getCinemaFacilities(Cinema cinema) {
		Map<String, Integer> fMap = new HashMap<String, Integer>();

		if (cinema.isHasSpecialChildPlay())
			fMap.put(getString(R.string.children_strict), new Integer(
					R.drawable.cinema_child));
		if (cinema.isHasSpecialDLP())
			fMap.put("DLP", new Integer(R.drawable.cinema_dlp));
		if (cinema.isHasSpecialDTS())
			fMap.put("DTS", new Integer(R.drawable.cinema_dts));
		if (cinema.isHasSpecialFoodCourt())
			fMap.put(getString(R.string.food_strict), new Integer(
					R.drawable.cinema_dinner));
		if (cinema.isHasSpecialGameRoom())
			fMap.put(getString(R.string.game_strict), new Integer(
					R.drawable.cinema_game));
		if (cinema.isHasSpecialHandicappedAccess())
			fMap.put(getString(R.string.disable_strict), new Integer(
					R.drawable.cinema_disable));
		if (cinema.isHasSpecialLadderChair())
			fMap.put(getString(R.string.stair_strict), new Integer(
					R.drawable.cinema_stairs));
		if (cinema.isHasSpecialPark())
			fMap.put(getString(R.string.park_strict), new Integer(
					R.drawable.cinema_stop));
		if (cinema.isHasSpecialSDDS())
			fMap.put("SDDS", new Integer(R.drawable.cinema_sdds));

		return fMap;
	}

	@Override
	void doOperation() {
		new Task() {
			Cinema cinema;

			@Override
			public void after() {
				if (null == cinema)
					MtimeUtils.gotoErrorActivity(CinemaDetailActivity.this);
				else
					setUpCinemaDetailView(cinema);
			}

			@Override
			public void before() {
				cinema = rs.getCinemaDetail(cinemaId);
			}
		}.start();

	}
}

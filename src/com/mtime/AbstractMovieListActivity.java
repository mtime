package com.mtime;

import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.mtime.Constants.ViewClass;
import com.mtime.data.Locations;
import com.mtime.util.MtimeUtils;

public abstract class AbstractMovieListActivity extends AbstractMtimeActivity {
	Button cinemaListBtn, myLocationBtn, comingMovieListBtn;
	TextView navTitleView;
	private ViewFlipper vflipper;
	private View locationSettingView;
	List<Locations> locations;

	ViewClass currentView = ViewClass.NOT_SET;
	ViewClass fromView = ViewClass.NOT_SET;

	@Override
	protected void onStart() {

		navTitleView = (TextView) findViewById(R.id.tv_NavTitle);

		vflipper = (ViewFlipper) findViewById(R.id.flipper);

		locationSettingView = getLayoutInflater().inflate(
				R.layout.view_bottom_nav, null);
		vflipper.addView(locationSettingView);
		vflipper.showNext();

		cinemaListBtn = (Button) locationSettingView
				.findViewById(R.id.btn_CinemaList);
		myLocationBtn = (Button) locationSettingView
				.findViewById(R.id.btn_MyPlace);
		comingMovieListBtn = (Button) locationSettingView
				.findViewById(R.id.btn_ComingMovie);

		((Button) locationSettingView.findViewById(R.id.btn_LocationSetting))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						showDialog(DIALOG_PLACE_SETTING);
					}
				});

		((TextView) locationSettingView.findViewById(R.id.tv_CityName))
				.setText(currentLocation.getName());

		if (currentView == ViewClass.MovieComingList) {
			locationSettingView.findViewById(R.id.btn_LocationSetting)
					.setVisibility(View.GONE);
			locationSettingView.findViewById(R.id.tv_CityName).setVisibility(
					View.GONE);

		}

		// Set other buttons
		cinemaListBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent i = new Intent();
				i.setClass(AbstractMovieListActivity.this,
						CinemaListActivity.class);

				int cinemaId = getIntent().getIntExtra(Constants.KEY_CINEMA_ID,
						-1);
				if (cinemaId != -1)
					i.putExtra(Constants.KEY_LOCATION_ID, currentLocation
							.getId());
				startActivity(i);

				if (currentView != ViewClass.MovieRecentList)
					finish();

			}
		});

		myLocationBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent i = new Intent();
				i.setClass(AbstractMovieListActivity.this,
						MyPlaceActivity.class);
				startActivity(i);
			}
		});

		comingMovieListBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent i = new Intent();
				i.setClass(AbstractMovieListActivity.this,
						MovieComingListActivity.class);

				startActivity(i);
				if (currentView != ViewClass.MovieRecentList)
					finish();

			}
		});

		super.onStart();
	}

	private Locations getCityId(String inputCity) throws Exception {

		if (null == locations) {
			locations = rs.getChinaLocations();
		}
		for (Locations province : locations) {
			// Try city first, if this is a zhixiashi, try it as well
			List<Locations> cities = province.getCities();
			if (null == cities || 0 == cities.size()) {
				if (inputCity.trim().startsWith(province.getName().trim()))
					return province;
			} else {
				for (Locations city : cities) {
					if (inputCity.trim().startsWith(city.getName().trim()))
						return city;
				}
			}
		}
		return null;
	}

	// Private class to get what the location is now
	public class LocationRefresher extends BaseLocationRefresher {
		public LocationRefresher(Activity activity) {
			super(activity);
		}

		@Override
		public void onLocationChanged(Location newLoc) {
			mIsLocationChanged = true;
			Log.d(Constants.LOGTAG, "The location is " + newLoc.getLatitude()
					+ " and " + newLoc.getLongitude());
			Geocoder coder = new Geocoder(mActivity, Locale.CHINA);
			mCondVar.open();
			try {
				List<Address> address = coder.getFromLocation(newLoc
						.getLatitude(), newLoc.getLongitude(), 1);
				Log.i(Constants.LOGTAG, "The location is "
						+ address.get(0).toString());
				String province = address.get(0).getAdminArea();
				String city = address.get(0).getLocality();
				updateProvinceAndCity(province, city);
			} catch (Exception e) {
				e.printStackTrace();
				MtimeUtils.showShortToastMessage(mActivity, getResources()
						.getString(R.string.no_network));
			}
		}
	}

	private void updateProvinceAndCity(String province, String city)
			throws Exception {
		if (null == locations) {
			locations = rs.getChinaLocations();
		}

		// Set selected province.
		String city_name = "";
		for (Locations location : locations) {
			if (province.trim().startsWith(location.getName().trim())) {
				List<Locations> cities = location.getCities();
				if (null != cities && 0 != cities.size()) {
					for (Locations now_city : cities) {
						if (city.trim().startsWith(now_city.getName().trim())) {
							city_name = now_city.getName().trim();
							break;
						}
					}
					// If there is no city name is set, use the first one.
					if ("" == city_name)
						city_name = cities.get(0).getName();
				} else {
					// Province name is the result;
					city_name = location.getName().trim();
				}
				break;
			}
		}
		// If no location is set, just set the default location.
		if ("" == city_name)
			city_name = getString(R.string.default_city);
		((TextView) locationSettingView.findViewById(R.id.tv_CityName))
				.setText(city_name);
		return;
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog dialog = super.onCreateDialog(id);
		if (dialog != null)
			return dialog;

		switch (id) {
		case DIALOG_PLACE_SETTING: {

			locationSettingView = getLayoutInflater().inflate(
					R.layout.view_location_dialog, null);
			final TextView cityName = ((TextView) locationSettingView
					.findViewById(R.id.tv_CityName));
			cityName.setText(currentLocation.getName());
			final LocationRefresher mLocationRefresher = new LocationRefresher(
					AbstractMovieListActivity.this);

			((ImageButton) locationSettingView
					.findViewById(R.id.btn_CheckLocation))
					.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View arg0) {
							mLocationRefresher.refresh(5);
						}
					});

			return new AlertDialog.Builder(AbstractMovieListActivity.this)
					.setTitle(R.string.my_place).setView(locationSettingView)
					.setPositiveButton(R.string.ok,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int whichButton) {
									CharSequence input_city = cityName
											.getText();
									Locations city_location = null;
									try {
										city_location = getCityId(input_city
												.toString());
									} catch (Exception e) {
									}
									if (null == city_location) {
										MtimeUtils
												.showShortToastMessage(
														AbstractMovieListActivity.this,
														getString(R.string.wrong_input_city));
									} else {
										// Update city and location id

										MtimeUtils.setLocations(
												AbstractMovieListActivity.this,
												city_location);
										currentLocation = city_location;
										((TextView) findViewById(R.id.tv_CityName))
												.setText(currentLocation
														.getName());

										Intent i = new Intent();

										if (currentView == ViewClass.CinemaList) {
											// Fresh the cinemas list
											i
													.setClass(
															AbstractMovieListActivity.this,
															CinemaListActivity.class);
											i.putExtra(
													Constants.KEY_LOCATION_ID,
													currentLocation.getId());

											i.putExtra(Constants.KEY_FROM_VIEW,
													fromView);

											startActivity(i);

											if (currentView != ViewClass.MovieRecentList)
												finish();
										} else {
											// Just to recent cinemas.
											i.putExtra(Constants.KEY_CINEMA_ID,
													0);
											i.putExtra(
													Constants.KEY_LOCATION_ID,
													currentLocation.getId());
											i
													.setClass(
															AbstractMovieListActivity.this,
															MovieRecentListActivity.class);

											startActivity(i);

											if (currentView == ViewClass.MovieRecentList)
												finish();

										}

									}

								}
							}).setNegativeButton(R.string.cancel,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int whichButton) {
									// cancel
								}
							}).create();

		}
		case DIALOG_NO_MOVIE_IN_THIS_CINEMA: {
			return new AlertDialog.Builder(AbstractMovieListActivity.this)
					.setTitle(R.string.no_movie_in_this_cinema).setIcon(
							android.R.drawable.ic_dialog_info)
					.setNeutralButton(R.string.check_other_cinema,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int whichButton) {

									// Jump to all cinema's list
									Intent i = new Intent();
									i.setClass(AbstractMovieListActivity.this,
											CinemaListActivity.class);
									startActivity(i);

									if (currentView != ViewClass.MovieRecentList)
										finish();

								}
							}).create();

		}
		case DIALOG_NO_MOVIE_IN_THIS_CITY: {
			return new AlertDialog.Builder(AbstractMovieListActivity.this)
					.setMessage(R.string.no_movie_in_this_city).show();
		}
		}
		return null;
	}
}

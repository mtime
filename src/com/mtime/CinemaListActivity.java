package com.mtime;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.mtime.Constants.ViewClass;
import com.mtime.data.Cinema;
import com.mtime.util.MtimeUtils;

public class CinemaListActivity extends AbstractMovieListActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_cinema_list);
		currentView = ViewClass.CinemaList;
	}

	@Override
	protected void onStart() {
		super.onStart();
		navTitleView.setText(R.string.cinema_select);
	}

	private void showCinemaListView() {

		new Task() {
			List<Cinema> cinemaList;

			@Override
			public void after() {
				if (null == cinemaList)
					MtimeUtils.gotoErrorActivity(CinemaListActivity.this);
				else
					showCinemaListView(cinemaList);

			}

			@Override
			public void before() throws Exception {
				cinemaList = rs.getLocationCinemas(currentLocation.getId());

			}
		}.start();
	}

	private void showCinemaListView(final List<Cinema> cinemaList) {

		final ListView lv = (ListView) findViewById(R.id.lv_CinemaList);

		lv.setItemsCanFocus(false);
		lv.setAdapter(new ArrayAdapter<String>(this, R.layout.item_cinema,
				getList(cinemaList)));
		lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Cinema c = cinemaList.get(position);

				Intent i = new Intent();
				if (ViewClass.Settings == fromView) {
					c.setLocationId(currentLocation.getId());
					c.setLocationName(currentLocation.getName());
					MtimeUtils.setDefaultCinema(CinemaListActivity.this, c);
					i.setClass(CinemaListActivity.this, SettingsActivity.class);

				} else {
					i.setClass(CinemaListActivity.this,
							MovieRecentListActivity.class);

					i.putExtra(Constants.KEY_CINEMA_ID, c.getId());
					i.putExtra(Constants.KEY_CINEMA_NAME, c.getName());
					i.putExtra(Constants.KEY_LOCATION_ID, currentLocation
							.getId());
				}
				startActivity(i);
				finish();
			}

		});
	}

	private String[] getList(List<Cinema> list) {
		String[] array = new String[list.size()];
		for (int i = 0; i < list.size(); i++) {
			array[i] = list.get(i).getName();
		}
		return array;
	}

	@Override
	void doOperation() {
		fromView = (ViewClass) getIntent().getSerializableExtra(
				Constants.KEY_FROM_VIEW);

		if (ViewClass.Settings == fromView) {
			cinemaListBtn.setVisibility(View.GONE);
			myLocationBtn.setVisibility(View.GONE);
			comingMovieListBtn.setVisibility(View.GONE);
		}
		showCinemaListView();
	}
}
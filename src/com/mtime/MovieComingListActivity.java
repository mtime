package com.mtime;

import java.util.ArrayList;
import java.util.List;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.mtime.Constants.ViewClass;
import com.mtime.data.Movie;
import com.mtime.util.AsyncImageLoader;
import com.mtime.util.MtimeUtils;

public class MovieComingListActivity extends AbstractMovieListActivity {

	// private final String TAG = "MovieComingList";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_movie_coming_list);
		currentView = ViewClass.MovieComingList;
	}

	@Override
	protected void onStart() {
		super.onStart();
		navTitleView.setText(R.string.movie_coming);
	}

	private void showMovieComingListView() {

		new Task() {
			List<Movie> list;

			@Override
			public void after() {
				if (null == list)
					MtimeUtils.gotoErrorActivity(MovieComingListActivity.this);
				else
					showMovieComingListView(list);
			}

			@Override
			public void before() throws Exception {
				list = rs.getComingMovies();
			}
		}.start();
	}

	private void showMovieComingListView(List<Movie> list) {

		if (list.size() == 0) {
			emptyAlert.setText(R.string.no_coming_movie);
			emptyAlert.setVisibility(View.VISIBLE);
			return;
		} else {
			emptyAlert.setVisibility(View.GONE);
		}

		ListView lv = (ListView) findViewById(R.id.lv_ComingMovieList);
		lv.setItemsCanFocus(false);
		lv.setAdapter(new MovieComingAdapter(list));

	}

	public class MovieComingAdapter extends BaseAdapter {
		private final List<Movie> movieList;
		List<View> galleryViewList;

		public MovieComingAdapter(List<Movie> list) {
			this.movieList = list;
			galleryViewList = new ArrayList<View>(list.size());
			for (int i = 0; i < movieList.size(); i++)
				galleryViewList.add(null);
		}

		private View buildView(Movie m) {
			View movieView = getLayoutInflater().inflate(
					R.layout.item_movie_coming, null);
			((TextView) movieView.findViewById(R.id.tv_MovieName)).setText(m
					.getName());
			StringBuffer desc = new StringBuffer();
			desc.append(getString(R.string.coming) + " "
					+ MtimeUtils.getSimpleDate(m.getShowdate()) + "\n");
			desc.append(getString(R.string.director) + " "
					+ m.getDirectorName() + "\n");
			desc.append(getString(R.string.main_actor) + " "
					+ m.getActorName1() + " " + m.getActorName2());

			((TextView) movieView.findViewById(R.id.tv_MovieDesc)).setText(desc
					.toString());

			ImageView v = (ImageView) movieView.findViewById(R.id.icon);
			String imageUrl = m.getImageSrc();

			Drawable d = AsyncImageLoader.loadDrawable(imageUrl,
					new AsyncImageLoader.DefaultImageCallback(v),
					MovieComingListActivity.this);

			if (d == null)
				d = MovieComingListActivity.this.getResources().getDrawable(
						R.drawable.default_movie_post);
			v.setImageDrawable(d);

			return movieView;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = galleryViewList.get(position);
			if (v == null) {
				v = buildView(movieList.get(position));
				galleryViewList.set(position, v);

			}
			return v;
		}

		@Override
		public int getCount() {
			return movieList.size();
		}

		@Override
		public Object getItem(int position) {
			return movieList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}
	}

	

	@Override
	void doOperation() {
		showMovieComingListView();
	}
}
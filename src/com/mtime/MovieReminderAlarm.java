package com.mtime;

import java.util.List;

import org.apache.http.cookie.Cookie;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.mtime.util.MtimeUtils;

/**
 * When there's an alarm, show the notification in the status bar. When user
 * tries to click it, show the detail movie.
 * 
 * @author Dreamcast
 */
public class MovieReminderAlarm extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        // Added by dreamcast, to show movie reminder.
        // If user login and has some movie interested, will alarm user.
        List<Cookie> loginCookies = MtimeUtils.getCookies();
        if (null == loginCookies
                || (0 != loginCookies.size() && MtimeUtils.getNotifySetting(
                        context, Constants.KEY_SETTING_COMING_NOTIFY))) {
            // Cancel it in alarm manager.
            int alarmNumber = intent.getExtras().getInt(
                    Constants.KEY_ALARM_NUMBER);
            MtimeUtils.cancelEventInBroadcast(context, MovieReminderAlarm.class,
                    alarmNumber);

            String movieName = intent.getExtras().getString(
                    Constants.KEY_MOVIE_NAME);
            int movieId = intent.getExtras().getInt(Constants.KEY_MOVIE_ID);
            String cinemaName = intent.getExtras().getString(
                    Constants.KEY_CINEMA_NAME);
            int showtimeId = intent.getExtras().getInt(
                    Constants.KEY_SHOWTIME_ID);

            // look up the notification manager service
            NotificationManager notification = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            // The details of our fake message
            String from = context.getString(R.string.app_name);
            from = movieName;
            // CharSequence message = context.getString(R.string.movie_you_want)
            // + movieName + context.getString(R.string.is_coming);
            String message = intent.getExtras()
                    .getString(Constants.KEY_MESSAGE);

            // The PendingIntent to launch our activity if the user selects this
            // notification
            Intent goIntent = new Intent();
            goIntent.setClass(context, MovieSessionSelectedActivity.class);
            goIntent.putExtra(Constants.KEY_FROM_ALARM, true);
            goIntent.putExtra(Constants.KEY_MOVIE_ID, movieId);
            goIntent.putExtra(Constants.KEY_MOVIE_NAME, movieName);
            goIntent.putExtra(Constants.KEY_CINEMA_NAME, cinemaName);
            goIntent.putExtra(Constants.KEY_SHOWTIME_ID, showtimeId);
            goIntent.putExtra(Constants.KEY_ALARM_NUMBER, alarmNumber);
            goIntent.setData(Uri.parse(Constants.MTIME_ALARM + alarmNumber));

            PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                    goIntent, 0);
            // construct the Notification object.
            Notification notif = new Notification(R.drawable.stat_sample,
                    message, System.currentTimeMillis());

            // Set the info for the views that show in the notification panel.
            notif.setLatestEventInfo(context, from, message, contentIntent);

            // after a 100ms delay, vibrate for 250ms, pause for 100 ms and
            // then vibrate for 500ms.
            notif.vibrate = new long[] { 100, 250, 100, 500 };
            Log.d("Alarm", "MovieReminder:"
                    + (R.string.movie_reminder + movieId) + "," + movieId);
            notification.notify(alarmNumber, notif);
        }
    }
}

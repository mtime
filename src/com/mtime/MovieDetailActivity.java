package com.mtime;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.mtime.data.Comment;
import com.mtime.data.Movie;
import com.mtime.util.AsyncImageLoader;
import com.mtime.util.MtimeUtils;

public class MovieDetailActivity {

	public static void showMovieDetailView(final Activity act,
			final Movie movie, ViewFlipper vflipper) {

		View movieDetailView = act.getLayoutInflater().inflate(
				R.layout.view_movie_detail, null);

		ImageView moviePostIma;

		TextView ratingScore1, ratingScore2, ratingCount, moviePeoples, movieSummary;

		ratingScore1 = (TextView) movieDetailView
				.findViewById(R.id.tv_ratingScore1);
		ratingScore2 = (TextView) movieDetailView
				.findViewById(R.id.tv_ratingScore2);
		moviePeoples = (TextView) movieDetailView
				.findViewById(R.id.tv_moviePeoples);
		movieSummary = (TextView) movieDetailView
				.findViewById(R.id.tv_movieSummary);
		ratingCount = (TextView) movieDetailView
				.findViewById(R.id.tv_ratingCount);
		moviePostIma = (ImageView) movieDetailView
				.findViewById(R.id.iv_MoviePost);
		if (movie.getRatingScore() > 0) {
			ratingScore1.setText(String.valueOf(movie.getRatingScore())
					.substring(0, 1));
			ratingScore2.setText(String.valueOf(movie.getRatingScore())
					.substring(1, 3));
		}
		ratingCount.setText(movie.getRatingCount()
				+ act.getString(R.string.people_rating));
		movieSummary.setText(movie.getSummary().trim());
		String peoples = act.getString(R.string.director) + " "
				+ movie.getDirectorName() + "\n"
				+ act.getString(R.string.main_actor) + " "
				+ movie.getActorName1() + "\n　　 " + movie.getActorName2()
				+ "\n时长:" + movie.getDuration();

		moviePeoples.setText(peoples);

		String imageUrl = movie.getImageSrc();
		Drawable d = AsyncImageLoader.loadDrawable(imageUrl,
				new AsyncImageLoader.DefaultImageCallback(moviePostIma), act);

		if (d == null)
			d = act.getResources().getDrawable(R.drawable.default_movie_post);
		moviePostIma.setImageDrawable(d);

		Button btn = (Button) movieDetailView.findViewById(R.id.btn_trailer);
		btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent();
				i.setClass(act, MovieTrailerActivity.class);
				i.putExtra(Constants.KEY_MOVIE_TRAILER, movie.getTrailerSrc());
				act.startActivity(i);
			}
		});

		if (movie.getTrailerSrc() == null
				|| movie.getTrailerSrc().length() == 0) {
			btn.setEnabled(false);
		}

		List<Comment> comments = movie.getComments();
		LinearLayout commentLayout = (LinearLayout) movieDetailView
				.findViewById(R.id.comment);

		for (int i = 0; i < comments.size(); i++) {
			Comment c = comments.get(i);

			LinearLayout commentView = (LinearLayout) act.getLayoutInflater()
					.inflate(R.layout.item_movie_comment, null);

			TextView ratingScore = (TextView) commentView
					.findViewById(R.id.tv_ratingScore);
			String[] r = MtimeUtils.formatRatingScore(c.getRatingScore());
			ratingScore.setText("评分: " + r[0] + r[1]);

			final TextView comment = (TextView) commentView
					.findViewById(R.id.tv_comment);

			final int maxLength = 60;
			final Spanned commentText = Html.fromHtml(c.getContent());
			if (commentText.length() < maxLength) {
				comment.setText(commentText);
			} else {
				// TextView ratingScore = (TextView)
				// findViewById(R.id.rating_score);
				// SpannableStringBuilder ssb = new SpannableStringBuilder(Html
				// .fromHtml("7<sup>.2</sup>"));
				// ssb.setSpan(new RelativeSizeSpan(0.7f), 1, 3,
				// Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
				// ratingScore.setText(ssb);

				comment.setText(commentText.subSequence(0, maxLength - 10)
						+ "...[点击查看全部]");
				comment.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						if (comment.getText().length() == commentText.length())
							comment.setText(commentText.subSequence(0,
									maxLength - 10)
									+ "...[点击查看全部]");
						else
							comment.setText(commentText);
					}

				});
			}

			TextView author = (TextView) commentView
					.findViewById(R.id.tv_commentAuthor);
			author
					.setText(c.getAuthorNickname() + "(" + c.getLocation()
							+ ") ");

			ImageView icon = (ImageView) commentView.findViewById(R.id.icon);
			String imaUrl = c.getAuthorImgSrc();
			Drawable di = AsyncImageLoader.loadDrawable(imaUrl,
					new AsyncImageLoader.DefaultImageCallback(icon), act);

			if (di == null)
				di = act.getResources().getDrawable(
						R.drawable.default_user_post);
			icon.setImageDrawable(di);

			commentLayout.addView(commentView);

		}
		vflipper.addView(movieDetailView);
		vflipper.invalidate();
	}
}
package com.mtime;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.mtime.Constants.ViewClass;
import com.mtime.data.Cinema;
import com.mtime.data.SignInResult;
import com.mtime.util.FileLocalCache;
import com.mtime.util.MtimeUtils;

public class SettingsActivity extends AbstractMtimeActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.act_settings);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return false;
	}

	@Override
	void doOperation() {
		final Cinema defaultCinema = MtimeUtils.getDefaultCinema(this);
		String[] u = MtimeUtils.getLoginUser(this);
		String email = u[0];
		// String password = u[1];

		final EditText emailEditText = (EditText) findViewById(R.id.tv_Email);
		final TextView emailEditText2 = (TextView) findViewById(R.id.tv_Email2);

		final EditText passEditText = (EditText) findViewById(R.id.tv_Password);
		final Button loginButton = (Button) findViewById(R.id.btn_Login);
		final Button logoutButton = (Button) findViewById(R.id.btn_Logout);

		final Button defaultCinemaButton = (Button) findViewById(R.id.btn_SetDefaultCinema);
		final Button clearCacheButton = (Button) findViewById(R.id.btn_ClearCache);
		final String clearCache = getString(R.string.clear_cache);
		clearCacheButton.setText(clearCache.trim() + " ("
				+ getString(R.string.cache_so_far) + "："
				+ FileLocalCache.getCacheSize() + ")");

		clearCacheButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				FileLocalCache.clearCache();
				clearCacheButton.setText(clearCache + " ("
						+ getString(R.string.cache_so_far) + "："
						+ FileLocalCache.getCacheSize() + ")");
			}

		});

		if (defaultCinema.getId() > 0)
			defaultCinemaButton.setText(defaultCinema.getName());

		final ToggleButton ratingNotifyTButton = (ToggleButton) findViewById(R.id.tbtn_RatingNotify);
		final ToggleButton comingNotifyTButton = (ToggleButton) findViewById(R.id.tbtn_ComingNotify);
		ratingNotifyTButton.setChecked(MtimeUtils.getNotifySetting(this,
				Constants.KEY_SETTING_RATING_NOTIFY));
		comingNotifyTButton.setChecked(MtimeUtils.getNotifySetting(this,
				Constants.KEY_SETTING_COMING_NOTIFY));

		final View loginView = findViewById(R.id.view_Login);
		final View loginResultView = findViewById(R.id.view_LoginResult);

		if (email != null && email.length() > 0) {
			emailEditText2.setText(email);
			loginView.setVisibility(View.GONE);
			loginResultView.setVisibility(View.VISIBLE);
			defaultCinemaButton.setEnabled(true);
			ratingNotifyTButton.setEnabled(true);
			comingNotifyTButton.setEnabled(true);
		} else {
			loginView.setVisibility(View.VISIBLE);
			loginResultView.setVisibility(View.GONE);
			defaultCinemaButton.setEnabled(false);
			ratingNotifyTButton.setEnabled(false);
			comingNotifyTButton.setEnabled(false);
		}

		loginButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String email = emailEditText.getText().toString();
				String password = passEditText.getText().toString();

				try {
					// TODO Exception handler
					// Then get nearby cinema's
					SignInResult r = rs.getSignInResult(email, password);
					if (r.isSuccess()) {
						MtimeUtils.setLoginUser(SettingsActivity.this, email,
								password);
						emailEditText2.setText(email);
						loginView.setVisibility(View.GONE);
						loginResultView.setVisibility(View.VISIBLE);
						defaultCinemaButton.setEnabled(true);
						ratingNotifyTButton.setEnabled(true);
						comingNotifyTButton.setEnabled(true);

						ratingNotifyTButton.setChecked(true);
						comingNotifyTButton.setChecked(true);

					} else {
						MtimeUtils.setLoginUser(SettingsActivity.this, "", "");
						MtimeUtils.showShortToastMessage(SettingsActivity.this,
								r.getError());
						defaultCinemaButton.setEnabled(false);
						ratingNotifyTButton.setEnabled(false);
						comingNotifyTButton.setEnabled(false);
					}

					MtimeUtils.setNotifySetting(SettingsActivity.this,
							Constants.KEY_SETTING_RATING_NOTIFY,
							ratingNotifyTButton.isChecked()
									&& ratingNotifyTButton.isEnabled());

					MtimeUtils.setNotifySetting(SettingsActivity.this,
							Constants.KEY_SETTING_COMING_NOTIFY,
							comingNotifyTButton.isChecked()
									&& comingNotifyTButton.isEnabled());
				} catch (Exception e) {

				}

			}

		});

		logoutButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				MtimeUtils.setLoginUser(SettingsActivity.this, "", "");
				MtimeUtils.clearCookies();
				Cinema cinema = new Cinema();
				cinema.setId(-1);
				cinema.setName("");
				cinema.setLocationId(-1);
				cinema.setLocationName("");
				MtimeUtils.setDefaultCinema(SettingsActivity.this, cinema);
				emailEditText.setText("");
				passEditText.setText("");
				loginView.setVisibility(View.VISIBLE);
				loginResultView.setVisibility(View.GONE);
				defaultCinemaButton
						.setText(getString(R.string.set_default_cinema));
				defaultCinemaButton.setEnabled(false);
				ratingNotifyTButton.setEnabled(false);
				comingNotifyTButton.setEnabled(false);

				MtimeUtils.setNotifySetting(SettingsActivity.this,
						Constants.KEY_SETTING_RATING_NOTIFY,
						ratingNotifyTButton.isEnabled()
								&& ratingNotifyTButton.isChecked());

				MtimeUtils.setNotifySetting(SettingsActivity.this,
						Constants.KEY_SETTING_COMING_NOTIFY,
						comingNotifyTButton.isEnabled()
								&& comingNotifyTButton.isChecked());

			}

		});

		defaultCinemaButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent i = new Intent();
				i.setClass(SettingsActivity.this, CinemaListActivity.class);
				i.putExtra(Constants.KEY_FROM_VIEW, ViewClass.Settings);
				startActivity(i);
				finish();
			}

		});

		ratingNotifyTButton
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						MtimeUtils.setNotifySetting(SettingsActivity.this,
								Constants.KEY_SETTING_RATING_NOTIFY,
								ratingNotifyTButton.isChecked());
					}

				});

		comingNotifyTButton
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						MtimeUtils.setNotifySetting(SettingsActivity.this,
								Constants.KEY_SETTING_COMING_NOTIFY,
								comingNotifyTButton.isChecked());
					}

				});
	}
}
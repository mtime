package com.mtime;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.mtime.data.Cinema;
import com.mtime.remote.DataServiceFactory;
import com.mtime.remote.RemoteService;
import com.mtime.util.MtimeUtils;

public class MyPlaceActivity extends MapActivity {
	RemoteService rs = null;
	private MapView mapView;
	private MyLocationOverlay mMyLocationOverlay;
	private CinemasNearLocation cinemasLocation;
	private List<PoiOnMapView> mPoiOnMapViewList = new ArrayList<PoiOnMapView>();
	private PoiOnMapView mMyLocationOnMap;
	private final int MAX_DISTANCE_PIXEL = 30;
	private static final int POI_SHIFT_Y_PIXEL = -50;
	private int mActivePoiPosition = -1;
	List<Cinema> nearCinemas;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		rs = DataServiceFactory.getRemoteService(this);
		// Sets up the MapView, zoom controls and overlays
		setContentView(R.layout.act_my_place);

		mapView = (MapView) findViewById(R.id.mapview1);
		mMyLocationOverlay = new MyLocationOverlay(this, mapView);
		mMyLocationOverlay.runOnFirstFix(new Runnable() {
			public void run() {
				mapView.getController().animateTo(
						mMyLocationOverlay.getMyLocation());
			}
		});
		mapView.getOverlays().add(mMyLocationOverlay);
		mapView.getController().setZoom(16);
		// mapView.setClickable(true);
		// mapView.setEnabled(true);
		mapView.setBuiltInZoomControls(true);
		// Sets onTouch listener to map (to popup nearest POI)
		mapView.setOnTouchListener(new MyOnTouchListener());
		cinemasLocation = new CinemasNearLocation(this);
		cinemasLocation.refresh(10);
		
	}

	@Override
	public void onResume() {
		super.onResume();
		
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	public class CinemasNearLocation extends BaseLocationRefresher {
		public CinemasNearLocation(Activity activity) {
			super(activity);
		}

		@Override
		public void onLocationChanged(Location myLoc) {
			// Trying to get now location
			mIsLocationChanged = true;
			Log.d(Constants.LOGTAG, "The location is " + myLoc.getLatitude()
					+ " and " + myLoc.getLongitude());
			mCondVar.open();

			try {
				// TODO Exception handler
				// Then get nearby cinema's
				nearCinemas = rs.getNearByCinemas(myLoc.getLatitude(), myLoc
						.getLongitude());
			} catch (Exception e) {

			}

			mPoiOnMapViewList.clear();
			// Put all content into maps's view
			if (null == nearCinemas || 0 == nearCinemas.size()) {
				MtimeUtils.showShortToastMessage(mActivity, mActivity
						.getResources().getString(R.string.no_cinema_nearby));
			} else {
				for (int i = 0; i < nearCinemas.size(); i++) {
					Cinema nowCinema = nearCinemas.get(i);
					PoiOnMapView poiOnMapView = PoiMapViewUtil.showMapPoi(
							MyPlaceActivity.this, mapView, new MapPoi.Builder(
									nowCinema.getGeoLatitude(), nowCinema
											.getGeoLongitude(),
									R.drawable.cinema, false)
									.setPopupWindowTitle(nowCinema.getName())
									.setPopupWindowClickListener(
											new MyClickListener(nowCinema))
									// .setPopupWindowSnippet(nowCinema.getName())
									.setPopupWindowYShiftPixel(-10)
									.setClosable(false).build(), false);
					poiOnMapView.showPopup();
					mPoiOnMapViewList.add(poiOnMapView);
				}
			}
			// Show Mylocation
			mMyLocationOnMap = PoiMapViewUtil.showMapPoi(MyPlaceActivity.this,
					mapView, new MapPoi.Builder(myLoc.getLatitude(), myLoc
							.getLongitude(),
							R.drawable.my_location_marker_green, true)
							.setPopupWindowTitle(
									getString(R.string.now_location))
							.setPopupWindowSnippet(
									getString(R.string.now_location_detail))
							.build(), true);
			mMyLocationOnMap.showPopup();
			mapView.invalidate();
		}

	}

	private class MyClickListener implements OnClickListener {

		Cinema cinema;

		public MyClickListener(Cinema cinema) {
			this.cinema = cinema;
		}

		@Override
		public void onClick(View arg0) {
			hidePopup();
			Intent intent = new Intent();
			intent
					.setClass(MyPlaceActivity.this,
							MovieRecentListActivity.class);
			intent.putExtra(Constants.KEY_CINEMA_ID, cinema.getId());
			intent.putExtra(Constants.KEY_CINEMA_NAME, cinema.getName());
			// i.putExtra(Constants.KEY_LOCATION_ID, currentLocation.getId());

			startActivity(intent);
		}
	}

	private class MyOnTouchListener implements View.OnTouchListener {

		public MyOnTouchListener() {
		}

		@Override
		public boolean onTouch(View view, MotionEvent event) {
			// User tag the screen
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				GeoPoint p = mapView.getProjection().fromPixels(
						(int) event.getX(), (int) event.getY());
				int nearest = getNearestPoiIndex(p, MAX_DISTANCE_PIXEL);
				if (nearest >= 0) {
					// Hides previous popup or my location popup
					hidePopup();
					PoiOnMapView poiOnMap = mPoiOnMapViewList.get(nearest);
					poiOnMap.showPopup();
					poiOnMap.centerPoi(POI_SHIFT_Y_PIXEL);
					mActivePoiPosition = nearest;
					return false;
				}
			}
			return false;
		}

		private int getNearestPoiIndex(GeoPoint p, int maxDistancePixel) {
			if (nearCinemas == null)
				return -1;
			int size = nearCinemas.size();
			int nearestIndex = -1;
			int nearestDistance = Integer.MAX_VALUE;
			for (int i = 0; i < size; i++) {
				Cinema poi = nearCinemas.get(i);
				int distance = PoiMapViewUtil.getDistance(poi.getGeoLatitude(),
						poi.getGeoLongitude(), PoiMapViewUtil
								.getLatitudeFromGeoPoint(p), PoiMapViewUtil
								.getLongitudeFromGeoPoint(p));
				if (distance < nearestDistance) {
					nearestDistance = distance;
					nearestIndex = i;
				}
			}
			int pixel = (int) mapView.getProjection().metersToEquatorPixels(
					nearestDistance);
			return (pixel <= maxDistancePixel) ? nearestIndex : -1;
		}
	}

	private void hidePopup() {
		if (mActivePoiPosition >= 0) {
			PoiOnMapView poiOnMap = mPoiOnMapViewList.get(mActivePoiPosition);
			poiOnMap.hidePopup();
		}
		mMyLocationOnMap.hidePopup();
	}
}

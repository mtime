package com.mtime;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.MenuItem.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.mtime.data.Cinema;
import com.mtime.data.Locations;
import com.mtime.util.MtimeUtils;

public abstract class AbstractMtimeActivity extends AbstractMtimeHandleActivity {

	public static final int DIALOG_ERROR = 0;
	public static final int DIALOG_WATING = 1;
	public static final int DIALOG_PLACE_SETTING = 2;
	public static final int DIALOG_NO_MOVIE_IN_THIS_CINEMA = 3;
	public static final int DIALOG_NO_MOVIE_IN_THIS_CITY = 4;
	private static String errorMsg = "";

	public static final String DISMISS_DIALOG = "dismiss_dialog";
	public static final String SHOW_DIALOG = "show_dialog";

	Locations currentLocation;

	View mainBody;
	TextView emptyAlert;
	ViewFlipper alertBody;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		int locationId = getIntent().getIntExtra(Constants.KEY_LOCATION_ID, -1);

		// Go to specific location
		if (locationId != -1) {
			currentLocation = MtimeUtils.getLocation(this);
		} else {
			Cinema defaultCinema = MtimeUtils.getDefaultCinema(this);
			Log.e("CAO", "defaultCinema=" + defaultCinema.toString());
			if (defaultCinema != null && defaultCinema.getLocationId() > 0) {
				Log.e("CAO", "defaultCinema=" + defaultCinema.getLocationId());
				currentLocation = new Locations(defaultCinema.getLocationId(),
						defaultCinema.getLocationName());
			} else {
				currentLocation = MtimeUtils.getLocation(this);
				Log.e("CAO", "currentLocation=" + currentLocation.getId());
			}
		}

		if (currentLocation == null || currentLocation.getId() <= 0) {
			currentLocation = new Locations();
			currentLocation.setName(getString(R.string.default_city));
			currentLocation.setId(Integer
					.parseInt(getString(R.string.default_city_id)));
		}

	}

	@Override
	protected void onStart() {
		super.onStart();
		mainBody = findViewById(R.id.view_MainBody);
		alertBody = (ViewFlipper) findViewById(R.id.view_AlertBody);
		emptyAlert = (TextView) findViewById(R.id.tv_EmptyAlert);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_WATING: {
			ProgressDialog dialog = new ProgressDialog(this);
			dialog.setMessage(getString(R.string.please_wait));
			dialog.setIndeterminate(true);
			dialog.setCancelable(false);
			return dialog;
		}
		case DIALOG_ERROR: {
			return new AlertDialog.Builder(this).setTitle(errorMsg)
					.setPositiveButton(R.string.ok,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int whichButton) {
									finish();
								}
							}).create();
		}
		}
		return null;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);

		MenuItem home = menu.add("").setIcon(R.drawable.home);

		home.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			public boolean onMenuItemClick(MenuItem item) {
				Intent intent = new Intent();
				intent.setClass(AbstractMtimeActivity.this,
						MovieRecentListActivity.class);
				AbstractMtimeActivity.this.startActivityForResult(intent, 0);
				AbstractMtimeActivity.this.finish();
				return true;
			}
		});

		MenuItem settings = menu.add("").setIcon(
				android.R.drawable.ic_menu_preferences);

		settings.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			public boolean onMenuItemClick(MenuItem item) {
				Intent intent = new Intent();
				intent.setClass(AbstractMtimeActivity.this,
						SettingsActivity.class);
				AbstractMtimeActivity.this.startActivityForResult(intent, 0);
				return true;
			}
		});

		MenuItem about = menu.add("").setIcon(
				android.R.drawable.ic_menu_info_details);
		about.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			public boolean onMenuItemClick(MenuItem item) {
				Intent intent = new Intent();
				intent
						.setClass(AbstractMtimeActivity.this,
								AboutActivity.class);
				AbstractMtimeActivity.this.startActivityForResult(intent, 0);
				return true;
			}
		});
		return true;
	}

	public void showDialog0(int id) {
		Message message = handler.obtainMessage(0, String.valueOf(id));
		handler.sendMessage(message);

	}

	public void dismissDialog() {
		Message message = handler.obtainMessage(0, DISMISS_DIALOG);
		handler.sendMessage(message);
	}

	public void waiting() {
		mainBody.setVisibility(View.GONE);
		if (emptyAlert != null)
			emptyAlert.setVisibility(View.GONE);
		alertBody.removeAllViews();
		alertBody.addView(getLayoutInflater().inflate(R.layout.view_loading,
				null));
		alertBody.setVisibility(View.VISIBLE);

	}

	public void dissmissWaiting() {
		alertBody.setVisibility(View.GONE);

		if (emptyAlert == null || emptyAlert.getVisibility() != View.VISIBLE)
			mainBody.setVisibility(View.VISIBLE);
	}

	final Handler handler = new Handler() {
		@Override
		public void handleMessage(Message message) {
			if (String.class.isInstance(message.obj)) {
				String msg = (String) message.obj;
				if (msg.equals(String.valueOf(DIALOG_WATING))) {
					waiting();
				} else if (msg.equals(String.valueOf(DIALOG_ERROR))) {
					showDialog(DIALOG_ERROR);
				} else if (msg.equals(DISMISS_DIALOG)) {
					dissmissWaiting();
				}
			} else if (Task.class.isInstance(message.obj)) {
				Task msg = (Task) message.obj;
				msg.after();
				dismissDialog();

			}
		}
	};

	public abstract class Task extends Thread {
		public abstract void after();

		public abstract void before() throws Exception;

		@Override
		public final void run() {
			try {
				showDialog0(DIALOG_WATING);
				before();
				Message message = handler.obtainMessage(0, this);
				handler.sendMessage(message);
			} catch (Exception e) {
				e.printStackTrace();
				errorMsg = e.getMessage();
				showDialog0(DIALOG_ERROR);
			}
		}
	}
}

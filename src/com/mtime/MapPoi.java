package com.mtime;

import java.io.Serializable;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View.OnClickListener;

public class MapPoi implements Serializable {

	private static final long serialVersionUID = 1L;
	private double mLat;
	private double mLng;
	private int mIconResId;
	private boolean mCenterAlign;
	private CharSequence mTitle;
	private CharSequence mSnippet;
	private int mPopupYShiftPixel;
	private OnClickListener mPopupOnClickListener;
	private boolean mReverseGeocodingNeeded;
	private Bitmap mImage;
	private boolean mIsClosable;

	private MapPoi(double lat, double lng, int iconResId, boolean centerAlign) {
		mLat = lat;
		mLng = lng;
		mIconResId = iconResId;
		mCenterAlign = centerAlign;
		mTitle = null;
		mSnippet = null;
		mPopupYShiftPixel = 0;
		mPopupOnClickListener = null;
		mReverseGeocodingNeeded = false;
		mImage = null;
		mIsClosable = false;
	}

	/**
	 * A helper class to construct a {@link MapPoi} instance.
	 */
	public static class Builder {
		private final MapPoi mTarget;

		/**
		 * Creates a MapPoi builder with necessary information.
		 * 
		 * @param lat
		 *            latitude
		 * @param lng
		 *            longitude
		 * @param iconResId
		 *            the resources id of the drawable
		 * @param centerAlign
		 *            the lat/lng should be on the center of the drawable,
		 *            otherwise the center in bottom row of the drawable will be
		 *            used (useful for "pin"-like graphics)
		 */
		public Builder(double lat, double lng, int iconResId,
				boolean centerAlign) {
			mTarget = new MapPoi(lat, lng, iconResId, centerAlign);
		}

		/**
		 * Sets the popup window title.
		 * 
		 * @param title
		 *            the title
		 */
		public Builder setPopupWindowTitle(CharSequence title) {
			mTarget.mTitle = title;
			return this;
		}

		/**
		 * Sets the popup window snippet. Popup window will show the snippet
		 * when title is not empty.
		 * 
		 * @param snippet
		 *            the snippet
		 */
		public Builder setPopupWindowSnippet(CharSequence snippet) {
			mTarget.mSnippet = snippet;
			return this;
		}

		public Builder setReverseGeocodingNeeded(boolean needed) {
			mTarget.mReverseGeocodingNeeded = needed;
			return this;
		}

		/**
		 * @param popupYShiftPixel
		 *            the Y-shifting pixels of the popup window from the lat/lng
		 */
		public Builder setPopupWindowYShiftPixel(int popupYShiftPixel) {
			mTarget.mPopupYShiftPixel = popupYShiftPixel;
			return this;
		}

		/**
		 * Sets the onClickListener of the popup window.
		 * 
		 * @param listener
		 *            the onClick listener
		 */
		public Builder setPopupWindowClickListener(OnClickListener listener) {
			mTarget.mPopupOnClickListener = listener;
			return this;
		}

		/**
		 * Sets the image.
		 * 
		 * @param image
		 *            the image to display
		 */
		public Builder setImage(Bitmap image) {
			mTarget.mImage = image;
			return this;
		}

		public Builder setClosable(boolean isClosable) {
			mTarget.mIsClosable = isClosable;
			return this;
		}

		public MapPoi build() {
			return mTarget;
		}
	}

	public double getLatitude() {
		return mLat;
	}

	public double getLongitude() {
		return mLng;
	}

	public Drawable getDrawable(Context context) {
		return context.getResources().getDrawable(mIconResId);
	}

	public boolean isCenterAlign() {
		return mCenterAlign;
	}

	public CharSequence getTitle() {
		return mTitle;
	}

	public boolean hasTitle() {
		return (!TextUtils.isEmpty(mTitle));
	}

	public CharSequence getSnippet() {
		return mSnippet;
	}

	public boolean hasSnippet() {
		return TextUtils.isEmpty(mSnippet);
	}

	public int getPopupYShiftPixel() {
		return mPopupYShiftPixel;
	}

	public OnClickListener getPopupOnClickListener() {
		return mPopupOnClickListener;
	}

	public boolean isReverseGeocodingNeeded() {
		return mReverseGeocodingNeeded;
	}

	public Bitmap getImage() {
		return mImage;
	}

	public boolean hasImage() {
		return !(mImage == null);
	}

	public boolean isClosable() {
		return mIsClosable;
	}
}
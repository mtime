package com.mtime;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ToggleButton;

public class Mtime extends Activity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.main);

		final ToggleButton btn = (ToggleButton) findViewById(R.id.ToggleButton01);
		btn.setEnabled(false);
		btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

			}

		});
		((Button) findViewById(R.id.RecentMovieListBtn))
				.setOnClickListener(new TestOnClickListener(
						MovieRecentListActivity.class));

		((Button) findViewById(R.id.MovieCinemaListBtn))
				.setOnClickListener(new TestOnClickListener(
						MovieCinemaListActivity.class));

		((Button) findViewById(R.id.LoginBtn))
				.setOnClickListener(new TestOnClickListener(SettingsActivity.class));

		((Button) findViewById(R.id.MovieSessionSelectedBtn))
				.setOnClickListener(new TestOnClickListener(
						MovieSessionSelectedActivity.class));

		((Button) findViewById(R.id.WelcomeBtn))
				.setOnClickListener(new TestOnClickListener(
						WelcomeActivity.class));

		((Button) findViewById(R.id.CinemaDetailBtn))
				.setOnClickListener(new TestOnClickListener(
						CinemaDetailActivity.class));

		((Button) findViewById(R.id.CinemaLocationBtn))
				.setOnClickListener(new CinemaLocationListener());
	}

	class TestOnClickListener implements OnClickListener {
		Class<?> actClass;

		public TestOnClickListener(Class<?> clazz) {
			actClass = clazz;
		}

		@Override
		public void onClick(View v) {
			Intent i = new Intent();
			i.setClass(Mtime.this, actClass);

			i.putExtra(Constants.KEY_MOVIE_ID, 84699);
			i.putExtra(Constants.KEY_MOVIE_NAME, "Kongzi");
			i.putExtra(Constants.KEY_CINEMA_ID, 2368);
			i.putExtra(Constants.KEY_CINEMA_NAME, "Huilongguan");
			i.putExtra(Constants.KEY_SHOWTIME_ID, 20484304);
			i.putExtra(Constants.KEY_DATE, 20100127);

			Mtime.this.startActivity(i);
		}
	}

	class CinemaLocationListener implements OnClickListener {

		public void onClick(View v) {
			// Uses default if no business name
			String name = "Huilongguanxingmei";

			// Generates POI list to be displayed on Map
			ArrayList<MapPoi> poiList = new ArrayList<MapPoi>();
			poiList.add(new MapPoi.Builder(40.075822, 116.319122,
					R.drawable.my_location_marker_green, true)
					.setPopupWindowTitle(name).setPopupWindowSnippet(
							"My Location Now").build());

			// Starts the MapView
			Intent intent = new Intent(Mtime.this, CinemaLocationActivity.class);
			intent.putExtra(Constants.KEY_MAP_POI, poiList);
			Mtime.this.startActivity(intent);
		}

	}
}

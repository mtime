package com.mtime;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.VideoView;

public class MovieTrailerActivity extends Activity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.act_movie_trailer);

		String path = getIntent().getStringExtra(Constants.KEY_MOVIE_TRAILER);
		//path = "http://vf.mtime.com/act/temp/100129161243450419.mp4-muxed.mp4";

		Log.i("MPlayer", path);

		VideoView mVideoView = (VideoView) findViewById(R.id.surface);

		mVideoView.setVideoURI(Uri.parse(path));
		mVideoView.setMediaController(new MediaController(this));
		mVideoView.requestFocus();
		mVideoView.start();

	}
}
package com.mtime.data;

import java.io.Serializable;
import java.util.List;

public class Movie implements Serializable {
	private static final long serialVersionUID = 3073494386756229361L;

	// 影片id
	private int id;
	// 影片名
	private String name;
	// 影片评分
	private double ratingScore;
	// 影片评分人数
	private int ratingCount;
	// 影片图片src
	private String imageSrc;
	// 影片预告片src
	private String trailerSrc;
	// 导演名称
	private String directorName;
	// 主演1
	private String actorName1;
	// 主演2
	private String actorName2;
	// 放映时长
	private String duration;

	// 影片剧情
	private String summary;

	// 影片上映影院数
	private int cinemaCount;
	// 影片上映场数
	private int showtimeCount;
	// 用户态度 1, 想看
	private int userAttitude;
	// 上映日期
	private long showdate;

	// 是否是重点推荐，如果是1，表示是推荐影片
	private int def;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getShowdate() {
		return showdate;
	}

	public void setShowdate(long showdate) {
		this.showdate = showdate;
	}

	private List<Comment> comments;

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public int getCinemaCount() {
		return cinemaCount;
	}

	public void setCinemaCount(int cinemaCount) {
		this.cinemaCount = cinemaCount;
	}

	public int getShowtimeCount() {
		return showtimeCount;
	}

	public void setShowtimeCount(int showtimeCount) {
		this.showtimeCount = showtimeCount;
	}

	public int getUserAttitude() {
		return userAttitude;
	}

	public void setUserAttitude(int userAttitude) {
		this.userAttitude = userAttitude;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getRatingScore() {
		return ratingScore;
	}

	public void setRatingScore(double ratingScore) {
		this.ratingScore = ratingScore;
	}

	public int getRatingCount() {
		return ratingCount;
	}

	public void setRatingCount(int ratingCount) {
		this.ratingCount = ratingCount;
	}

	public String getImageSrc() {
		return imageSrc;
	}

	public void setImageSrc(String imageSrc) {
		this.imageSrc = imageSrc;
	}

	public String getTrailerSrc() {
		return trailerSrc;
	}

	public void setTrailerSrc(String trailerSrc) {
		this.trailerSrc = trailerSrc;
	}

	public String getDirectorName() {
		return directorName;
	}

	public void setDirectorName(String directorName) {
		this.directorName = directorName;
	}

	public String getActorName1() {
		return actorName1;
	}

	public void setActorName1(String actorName1) {
		this.actorName1 = actorName1;
	}

	public String getActorName2() {
		return actorName2;
	}

	public void setActorName2(String actorName2) {
		this.actorName2 = actorName2;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public void setDef(int def) {
		this.def = def;
	}

	public int getDef() {
		return def;
	}
}

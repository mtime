package com.mtime.data;

import java.io.Serializable;

public class Showtime implements Serializable, Comparable {

	private static final long serialVersionUID = 1L;

	private int id;

	// 电影放映时间
	private long time;
	// 票价
	private String price;

	// 语言:"无","中文版","原版","粤语版","不详"
	private String language;

	// 版本:"无", "胶片", "数字", "Imax", "3D"
	private String version;

	// 放映时长，可能为片长加广告长度。单位:分钟
	private int duration;

	// 影厅名
	private String hall;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getHall() {
		return hall;
	}

	public void setHall(String hall) {
		this.hall = hall;
	}

	@Override
	public int compareTo(Object another) {
		Showtime s = (Showtime) another;
		return (int) (this.getTime() - s.getTime());
	}
}

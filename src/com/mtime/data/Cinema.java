package com.mtime.data;

public class Cinema {
	// 影院id
	private int id;
	// 影院名
	private String name;
	// 影院地址
	private String address;
	// 影院电话
	private String tel;
	// 影院邮编
	private String postCode;
	// 地理纬度
	private double geoLatitude;
	// 地理经度
	private double geoLongitude;
	// 行车路线
	private String route;
	// 影院总评分
	private double ratingScore;
	// 影院周边评分
	private double ratingScore4Arround;
	// 影院设施评分
	private double ratingScore4Equipment;
	// 影院舒适评分
	private double ratingScore4Comfort;
	// 影院交通评分
	private double ratingScore4Traffic;
	// 影院音响评分
	private double ratingScore4Sound;
	// 影院服务评分
	private double ratingScore4Service;
	// 残疾人通道
	private boolean hasSpecialHandicappedAccess;
	// 停车场
	private boolean hasSpecialPark;
	// 儿童娱乐区
	private boolean hasSpecialChildPlay;
	// 餐饮区
	private boolean hasSpecialFoodCourt;
	// 台阶式座椅
	private boolean hasSpecialLadderChair;
	// 游戏厅
	private boolean hasSpecialGameRoom;
	// SDDS
	private boolean hasSpecialSDDS;
	// DTS
	private boolean hasSpecialDTS;
	// DLP
	private boolean hasSpecialDLP;
	// 影院公告
	private String announce;

	// 影院所在地区名称
	private String locationName;
	// 影院所在地区编号
	private int locationId;

	// 某影片剩余场次
	private int remainingShowtimeCount;

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public int getRemainingShowtimeCount() {
		return remainingShowtimeCount;
	}

	public void setRemainingShowtimeCount(int remainingShowtimeCount) {
		this.remainingShowtimeCount = remainingShowtimeCount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		if(tel != null) {
			tel = tel.replace("－", "-");
			tel = tel.replace(" ", " / ");
		}
		this.tel = tel;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public double getGeoLatitude() {
		return geoLatitude;
	}

	public void setGeoLatitude(double geoLatitude) {
		this.geoLatitude = geoLatitude;
	}

	public double getGeoLongitude() {
		return geoLongitude;
	}

	public void setGeoLongitude(double geoLongitude) {
		this.geoLongitude = geoLongitude;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public double getRatingScore() {
		return ratingScore;
	}

	public void setRatingScore(double ratingScore) {
		this.ratingScore = ratingScore;
	}

	public double getRatingScore4Arround() {
		return ratingScore4Arround;
	}

	public void setRatingScore4Arround(double ratingScore4Arround) {
		this.ratingScore4Arround = ratingScore4Arround;
	}

	public double getRatingScore4Equipment() {
		return ratingScore4Equipment;
	}

	public void setRatingScore4Equipment(double ratingScore4Equipment) {
		this.ratingScore4Equipment = ratingScore4Equipment;
	}

	public double getRatingScore4Comfort() {
		return ratingScore4Comfort;
	}

	public void setRatingScore4Comfort(double ratingScore4Comfort) {
		this.ratingScore4Comfort = ratingScore4Comfort;
	}

	public double getRatingScore4Traffic() {
		return ratingScore4Traffic;
	}

	public void setRatingScore4Traffic(double ratingScore4Traffic) {
		this.ratingScore4Traffic = ratingScore4Traffic;
	}

	public double getRatingScore4Sound() {
		return ratingScore4Sound;
	}

	public void setRatingScore4Sound(double ratingScore4Sound) {
		this.ratingScore4Sound = ratingScore4Sound;
	}

	public double getRatingScore4Service() {
		return ratingScore4Service;
	}

	public void setRatingScore4Service(double ratingScore4Service) {
		this.ratingScore4Service = ratingScore4Service;
	}

	public boolean isHasSpecialHandicappedAccess() {
		return hasSpecialHandicappedAccess;
	}

	public void setHasSpecialHandicappedAccess(
			boolean hasSpecialHandicappedAccess) {
		this.hasSpecialHandicappedAccess = hasSpecialHandicappedAccess;
	}

	public boolean isHasSpecialPark() {
		return hasSpecialPark;
	}

	public void setHasSpecialPark(boolean hasSpecialPark) {
		this.hasSpecialPark = hasSpecialPark;
	}

	public boolean isHasSpecialChildPlay() {
		return hasSpecialChildPlay;
	}

	public void setHasSpecialChildPlay(boolean hasSpecialChildPlay) {
		this.hasSpecialChildPlay = hasSpecialChildPlay;
	}

	public boolean isHasSpecialFoodCourt() {
		return hasSpecialFoodCourt;
	}

	public void setHasSpecialFoodCourt(boolean hasSpecialFoodCourt) {
		this.hasSpecialFoodCourt = hasSpecialFoodCourt;
	}

	public boolean isHasSpecialLadderChair() {
		return hasSpecialLadderChair;
	}

	public void setHasSpecialLadderChair(boolean hasSpecialLadderChair) {
		this.hasSpecialLadderChair = hasSpecialLadderChair;
	}

	public boolean isHasSpecialGameRoom() {
		return hasSpecialGameRoom;
	}

	public void setHasSpecialGameRoom(boolean hasSpecialGameRoom) {
		this.hasSpecialGameRoom = hasSpecialGameRoom;
	}

	public boolean isHasSpecialSDDS() {
		return hasSpecialSDDS;
	}

	public void setHasSpecialSDDS(boolean hasSpecialSDDS) {
		this.hasSpecialSDDS = hasSpecialSDDS;
	}

	public boolean isHasSpecialDTS() {
		return hasSpecialDTS;
	}

	public void setHasSpecialDTS(boolean hasSpecialDTS) {
		this.hasSpecialDTS = hasSpecialDTS;
	}

	public boolean isHasSpecialDLP() {
		return hasSpecialDLP;
	}

	public void setHasSpecialDLP(boolean hasSpecialDLP) {
		this.hasSpecialDLP = hasSpecialDLP;
	}

	public String getAnnounce() {
		return announce;
	}

	public void setAnnounce(String announce) {
		this.announce = announce;
	}

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}
}

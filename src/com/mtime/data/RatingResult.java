package com.mtime.data;

public class RatingResult {

	// 错误信息: 当发生错误时，会输出提示信息
	private String error;
	// 当前电影的总评分
	private double ratingScore;
	// 当前电影的总评分人数
	private int ratingCount;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public double getRatingScore() {
		return ratingScore;
	}

	public void setRatingScore(double ratingScore) {
		this.ratingScore = ratingScore;
	}

	public int getRatingCount() {
		return ratingCount;
	}

	public void setRatingCount(int ratingCount) {
		this.ratingCount = ratingCount;
	}

}

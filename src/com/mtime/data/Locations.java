package com.mtime.data;

import java.util.List;

public class Locations {
	// 地区编号
	private int id;

	// 地区名称
	private String name;

	// 城市列表
	private List<Locations> cities;

	public Locations() {
	}

	public Locations(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Locations> getCities() {
		return cities;
	}

	public void setCities(List<Locations> cities) {
		this.cities = cities;
	}
}

package com.mtime.data;

public class Comment {
	// 评论标题
	// private String title;
	// 评论内容
	private String content;
	// 评论者昵称
	private String authorNickname;
	// 评论时间
	private long timestamp;
	// 评论者头像
	private String authorImgSrc;
	// 评论者所在城市
	private String location;
	// 评论者评分
	private double ratingScore;

	// public String getTitle() {
	// return title;
	// }
	//
	// public void setTitle(String title) {
	// this.title = title;
	// }

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAuthorNickname() {
		return authorNickname;
	}

	public void setAuthorNickname(String authorNickname) {
		this.authorNickname = authorNickname;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getAuthorImgSrc() {
		return authorImgSrc;
	}

	public void setAuthorImgSrc(String authorImgSrc) {
		this.authorImgSrc = authorImgSrc;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setRatingScore(double ratingScore) {
		this.ratingScore = ratingScore;
	}

	public double getRatingScore() {
		return ratingScore;
	}

}

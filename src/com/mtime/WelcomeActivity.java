package com.mtime;

import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.cookie.Cookie;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.mtime.data.Cinema;
import com.mtime.data.Movie;
import com.mtime.data.SignInResult;
import com.mtime.util.AsyncImageLoader;
import com.mtime.util.FileLocalCache;
import com.mtime.util.MtimeUtils;

public class WelcomeActivity extends AbstractMtimeHandleActivity {

    private static final int DIALOG_ALERT = 1;
    private static final int DIALOG_SHOWN_ALERT = 2;
    private boolean hasJumped = false;

    private ArrayList<Movie> shownAlertMovies = new ArrayList<Movie>();

    private ImageView adsImage;
    private String adsImageUrl;

    private Animation fadein;
    private Animation fadeout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.act_welcome);

        fadeout = AnimationUtils.loadAnimation(WelcomeActivity.this,
                R.anim.fadeout);
        fadein = AnimationUtils.loadAnimation(WelcomeActivity.this,
                R.anim.fadein);


    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case DIALOG_ALERT: {
            AlertDialog dialog = new AlertDialog.Builder(this).create();
            dialog.setTitle(getString(R.string.alert));
            dialog.setMessage(this.getResources().getText(
                    R.string.no_connections));
            // dialog.setCancelable(false);
            Window w = dialog.getWindow();
            WindowManager.LayoutParams wl = w.getAttributes();
            wl.y = 120;
            w.setAttributes(wl);
            return dialog;
        }
        case DIALOG_SHOWN_ALERT: {
            ListView lv = new ListView(WelcomeActivity.this);
            lv.setItemsCanFocus(false);
            lv.setAdapter(new MovieShownAlertAdapter(shownAlertMovies));

            lv.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                        int position, long id) {
                    Movie m = shownAlertMovies.get(position);

                    Intent i = new Intent();
                    i.setClass(WelcomeActivity.this,
                            MovieCinemaListActivity.class);

                    i.putExtra(Constants.KEY_MOVIE_ID, m.getId());
                    i.putExtra(Constants.KEY_MOVIE_NAME, m.getName());

                    i.putExtra(Constants.KEY_CINEMA_ID, 0);
                    i.putExtra(Constants.KEY_CINEMA_NAME, "");

                    startActivity(i);
                }

            });

            AlertDialog dialog = new AlertDialog.Builder(this).setTitle(
                    getString(R.string.you_movie_is_coming)).setView(lv)
                    .setPositiveButton(R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int whichButton) {
                                    Intent i = new Intent();
                                    i.setClass(WelcomeActivity.this,
                                            MovieRecentListActivity.class);
                                    startActivity(i);
                                    finish();

                                }
                            }).create();

            return dialog;
        }
        }
        return null;
    }

    Handler adsImageHandler = new Handler() {
        // @Override
        public void handleMessage(Message msg) {
            WelcomeActivity.this.adsImage = (ImageView) findViewById(R.id.imageAds);
            // set last time ads image
            adsImageUrl = MtimeUtils.getAdsImage(WelcomeActivity.this);

            // detect network connection, if there is not any connections
            // just stop at this screen
            ConnectivityManager cm = (ConnectivityManager) WelcomeActivity.this
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netinfo = cm.getActiveNetworkInfo();

            if (netinfo == null) {
                showDialog(DIALOG_ALERT);
            } else {
                // trying to download new one
                new Thread(new ImageAdsDownloader()).start();

                if (adsImageUrl != null && adsImageUrl.length() > 0) {
                    // InputStream image = FileLocalCache.load(adsImageUrl);
                    Drawable d = AsyncImageLoader
                            .loadDrawable(adsImageUrl,
                                    new AsyncImageLoader.DefaultImageCallback(
                                            adsImage), WelcomeActivity.this);
                    adsImage.setImageDrawable(d);

                    fadein = AnimationUtils.loadAnimation(WelcomeActivity.this,
                            R.anim.fadein);
                    adsImage.startAnimation(fadein);
                }

                boolean binding = false;
                SignInResult r = null;
                String[] user = MtimeUtils.getLoginUser(WelcomeActivity.this);
                if (!"".equals(user[0]) && !"".equals(user[1])) {
                    binding = true;

                    try {
                        // TODO Exception handler
                        // Then get nearby cinema's
                        r = rs.getSignInResult(user[0], user[1]);
                    } catch (Exception e) {

                    }
                }

                // if not binding or logged in successfully
                Intent i = new Intent();
                if (!binding || (r != null && r.isSuccess())) {
                    i.setClass(WelcomeActivity.this.getApplicationContext(),
                            MovieRecentListActivity.class);
                } else {
                    // loggin failed, clear cookies
                    MtimeUtils.clearCookies();
                    i.setClass(WelcomeActivity.this.getApplicationContext(),
                            SettingsActivity.class);
                }
                new Thread(new GoToApp(i)).start();
            }
        }
    };

    class ImageAdsRunner implements Runnable {
        // @Override
        public void run() {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            WelcomeActivity.this.adsImageHandler.sendMessage(new Message());
        }
    }

    Handler adsImageDownloaderHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Bundle data = msg.getData();
            String adsImageUrl = data.getString("adsImageUrl");
            if (adsImageUrl != null) {
                WelcomeActivity.this.adsImageUrl = adsImageUrl;
            }
        }
    };

    class ImageAdsDownloader implements Runnable {
        // @Override
        @SuppressWarnings("unchecked")
        public void run() {
            Cinema defaultCinema = MtimeUtils
                    .getDefaultCinema(WelcomeActivity.this);

            int locationId = 0;
            if (defaultCinema == null || defaultCinema.getLocationId() <= 0) {
                locationId = MtimeUtils.getLocation(WelcomeActivity.this)
                        .getId();

            } else {
                locationId = defaultCinema.getLocationId();
            }
            Map<String, Object> r = null;

            try {
                // TODO Exception handler
                // Then get nearby cinema's
                r = rs.getAdsImageAndLocationMovies(locationId);
            } catch (Exception e) {

                r = new HashMap<String, Object>();
                e.printStackTrace();
            }

            String adsImageUrl = (String) r.get("adsImage");
            List<Movie> movies = (List<Movie>) r.get("movies");
            // save ads image to cache
            if (adsImageUrl != null && adsImageUrl.length() > 0
                    && !adsImageUrl.equals(WelcomeActivity.this.adsImageUrl)) {
                InputStream inputStream;
                try {
                    inputStream = new URL(adsImageUrl).openStream();
                    inputStream = FileLocalCache.save(adsImageUrl, inputStream);
                    MtimeUtils.setAdsImage(WelcomeActivity.this, adsImageUrl);

                    Message message = new Message();
                    Bundle data = new Bundle();
                    data.putString("adsImageUrl", adsImageUrl);
                    data.putSerializable("movies", (Serializable) movies);
                    message.setData(data);
                    WelcomeActivity.this.adsImageDownloaderHandler
                            .sendMessage(message);

                } catch (Exception e) {
//                    MtimeUtils.setAdsImage(WelcomeActivity.this, "");
                    Log.e("AD", adsImageUrl);
                    e.printStackTrace();
                }
            } else if(adsImageUrl == null || "".equals(adsImageUrl)) {
                MtimeUtils.setAdsImage(WelcomeActivity.this, "");
            }

            // Added by dreamcast, to show movie reminder.
            // If user login and has some movie interested, will alarm user.
            List<Cookie> loginCookies = MtimeUtils.getCookies();
            if (null != loginCookies
                    && 0 != loginCookies.size()
                    && MtimeUtils.getNotifySetting(WelcomeActivity.this,
                            Constants.KEY_SETTING_COMING_NOTIFY)) {

                // If user interested on this, alarm him.
                for (Movie m : movies) {
                    if (1 == m.getUserAttitude()) {
                        shownAlertMovies.add(m);
                    }
                }
                if (shownAlertMovies.size() > 0) {

                    shownDialogHandler.sendMessage(new Message());

                }
            }
            // *****************end of movie reminder ****************
        }
    }

    Handler activitySwitcher = new Handler() {
        /** Gets called on every message that is received */
        // @Override
        public void handleMessage(Message msg) {
            if (!hasJumped) {
                hasJumped = true;

                adsImage.startAnimation(fadeout);
                Intent i = (Intent) msg.getData().getParcelable("intent");
                WelcomeActivity.this.startActivity(i);
                finish();
            }

        }
    };

    Handler shownDialogHandler = new Handler() {
        /** Gets called on every message that is received */
        // @Override
        public void handleMessage(Message msg) {
            if (!hasJumped) {
                hasJumped = true;
                showDialog(DIALOG_SHOWN_ALERT);
            }

        }
    };

    class GoToApp implements Runnable {
        private Intent i;

        public GoToApp(Intent i) {
            this.i = i;
        }

        // @Override
        public void run() {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Message msg = new Message();
            Bundle data = new Bundle();
            data.putParcelable("intent", (Parcelable) this.i);
            msg.setData(data);
            WelcomeActivity.this.activitySwitcher.sendMessage(msg);
        }
    }

    public class MovieShownAlertAdapter extends BaseAdapter {
        private final List<Movie> movieList;
        List<View> galleryViewList;

        public MovieShownAlertAdapter(List<Movie> list) {
            this.movieList = list;
            galleryViewList = new ArrayList<View>(list.size());
            for (int i = 0; i < movieList.size(); i++)
                galleryViewList.add(null);
        }

        private View buildView(Movie m) {
            View movieView = getLayoutInflater().inflate(
                    R.layout.item_movie_coming, null);

            String t = m.getName();
            if (m.getRatingScore() > 0) {
                t = t + " " + m.getRatingScore();
            }
            ((TextView) movieView.findViewById(R.id.tv_MovieName)).setText(t);
            StringBuffer desc = new StringBuffer();
            desc.append(getString(R.string.director) + " "
                    + m.getDirectorName() + "\n");
            desc.append(getString(R.string.main_actor) + " "
                    + m.getActorName1() + " " + m.getActorName2() + "\n");

            desc.append(getString(R.string.duration) + " " + m.getDuration()
                    + "\n");
            desc.append(m.getCinemaCount() + getString(R.string.cinema_show)
                    + m.getShowtimeCount() + getString(R.string.shows));

            ((TextView) movieView.findViewById(R.id.tv_MovieDesc)).setText(desc
                    .toString());

            ImageView v = (ImageView) movieView.findViewById(R.id.icon);
            String imageUrl = m.getImageSrc();

            Drawable d = AsyncImageLoader.loadDrawable(imageUrl,
                    new AsyncImageLoader.DefaultImageCallback(v),
                    WelcomeActivity.this);

            if (d == null)
                d = WelcomeActivity.this.getResources().getDrawable(
                        R.drawable.default_movie_post);
            v.setImageDrawable(d);

            return movieView;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = galleryViewList.get(position);
            if (v == null) {
                v = buildView(movieList.get(position));
                galleryViewList.set(position, v);

            }
            return v;
        }

        @Override
        public int getCount() {
            return movieList.size();
        }

        @Override
        public Object getItem(int position) {
            return movieList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        hasJumped = false;
    }

    @Override
    void doOperation() {
        new Thread(new ImageAdsRunner()).start();
    }
}
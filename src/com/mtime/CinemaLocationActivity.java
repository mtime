package com.mtime;

import java.util.ArrayList;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;

import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;

public class CinemaLocationActivity extends MapActivity {
	@SuppressWarnings("unchecked")
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_cinema_location);
		
		// Gets location, popup title and snippet
		Intent intent = getIntent();
		if (intent == null)
			throw new NullPointerException("no intent");

		// Gets the POI list
		ArrayList<MapPoi> mapPoiList = (ArrayList<MapPoi>) intent
				.getSerializableExtra(Constants.KEY_MAP_POI);
		if (mapPoiList.size() < 1)
			throw new RuntimeException("no map POI");

		// Sets up the MapView, zoom controls and overlays
		
		MapView mapView = (MapView) findViewById(R.id.mapview1);
		
		// Shows location, centered the first POI
		PoiMapViewUtil.showMapPoi(this, mapView, mapPoiList.get(0), true, 16)
				.showPopup();
		for (int i = 1; i < mapPoiList.size(); i++) {
			PoiMapViewUtil.showMapPoi(this, mapView, mapPoiList.get(i), false)
					.showPopup();
		}

		// Sets onTouch listener
		// mapView.setOnTouchListener(new MapViewOnTouchListener(mapView));
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}
}

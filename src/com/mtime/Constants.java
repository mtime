package com.mtime;

public class Constants {
	public static enum ViewClass {
		MovieRecentList, CinemaList, MovieComingList, Settings, NOT_SET
	};

	public static final int APPID = 2;
	public static final String CLIENT_KEY = "E80522235DEA4B72AE7C1659692A17BB";

	public static final String LOGTAG = "mtime";

	public static final String PROVINCE = "province";
	public static final String CITY = "city";
	public static final String CITY_CODE = "city_code";

	public static final String COOKIES = "cookies";

	public static final String COOKIES_STORE_PATH = "/data/data/com.mtime/files/cookies";
	public static final String CACHE_STORE_PATH = "/sdcard/Mtime/cache/";

	public static final String EMAIL = "email";
	public static final String PASSWORD = "password";

	public static final String DEFAULT_CINEMA_ID = "default_cinema_id";
	public static final String DEFAULT_CINEMA_NAME = "default_cinema_name";
	public static final String DEFAULT_LOCATION_ID = "default_location_id";
	public static final String DEFAULT_LOCATION_NAME = "default_location_name";

	public static final String KEY_DATE = "date";

	// keys used to pass to CinemaLocationActivity
	public static final String KEY_MAP_POI = "map_poi_list";
	// keys used to pass to MovieSessionDetailActivity

	public static final String KEY_FROM_VIEW = "from_view";

	public static final String KEY_SETTING_RATING_NOTIFY = "settings_rating_notify";
	public static final String KEY_SETTING_COMING_NOTIFY = "settings_coming_notify";

	public static final String KEY_MOVIE_NAME = "movie_name";
	public static final String KEY_MESSAGE = "message";
	public static final String KEY_MOVIE_ID = "movie_id";
	public static final String KEY_MOVIE_SRC = "movie_src";
	public static final String KEY_MOVIE_TRAILER = "movie_trailer";

	public static final String KEY_CINEMA_NAME = "cinema_name";
	public static final String KEY_CINEMA_ID = "cinema_id";
	public static final String KEY_LOCATION_ID = "location_id";

	public static final String KEY_SHOWTIME_ID = "showtime_id";
	public static final String KEY_SHOWTIME_LIST = "showtime_list";
	public static final String KEY_POS_IN_SHOWTIME_LIST = "pos_in_showtime_list";
	
	public static final String KEY_FROM_ALARM = "from_alarm";

	public static final int E2 = 100;
	public static final int E6 = 1000000;

	public static final String MOVIE_DETAIL = "movie_detail";

	public static final String ADS_IMAGE = "ads_image";
	
	public static final String KEY_ALARM_NUMBER = "key_alarm_number";
	public static final String MTIME_ALARM = "http://www.mtime.com/";

	// Mock data or real data
	public static enum DATA_TYPE {
		REAL_DATA, MOCK_DATA
	};

	// public static DATA_TYPE NOW_MODE = DATA_TYPE.MOCK_DATA;
	public static DATA_TYPE NOW_MODE = DATA_TYPE.REAL_DATA;
}

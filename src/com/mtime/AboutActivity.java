package com.mtime;

import android.os.Bundle;
import android.view.Menu;
import android.view.WindowManager;

public class AboutActivity extends AbstractMtimeActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.act_about);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return false;
	}

	@Override
	void doOperation() {
		// TODO Auto-generated method stub
		
	}
}
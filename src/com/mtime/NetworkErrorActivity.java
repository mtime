package com.mtime;

import android.app.Activity;
import android.os.Bundle;

public class NetworkErrorActivity extends Activity
{
    /**
     * Default activity when there is network related error.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_network_error);
    }
}
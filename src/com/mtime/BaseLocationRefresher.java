package com.mtime;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.ConditionVariable;
import android.util.Log;

import com.mtime.util.MtimeUtils;

public abstract class BaseLocationRefresher implements Runnable, LocationListener{
    long mTimeOutSec;
    Activity mActivity;
    boolean mIsLocationChanged = false;
    ConditionVariable mCondVar = new ConditionVariable();
    LocationManager mRefresherLocationManager;

    public BaseLocationRefresher(Activity activity) {
        mActivity = activity;
        mRefresherLocationManager = (LocationManager) mActivity
                .getSystemService(Context.LOCATION_SERVICE);
    }

    public void refresh(int timeOutSec) {
        mTimeOutSec = timeOutSec;
        Thread updateThread = new Thread(this);
        updateThread.setPriority(Thread.MIN_PRIORITY);
        updateThread.start();
    }

    @Override
    public void run() {
        MtimeUtils.showShortToastMessage(mActivity, mActivity.getResources()
                .getString(R.string.try_get_location));
        // Gets all providers for trying
        List<String> providerList = mRefresherLocationManager
                .getProviders(true);
        if (providerList.isEmpty()) {
            MtimeUtils.showShortToastMessage(mActivity, mActivity.getResources()
                    .getString(R.string.no_location_device));
            return;
        }

        // Tries all providers
        mIsLocationChanged = false;
        for (String provider : providerList) {
            requestLocation(provider);
            if (mIsLocationChanged) {
                break;
            }
        }
    }

    private boolean requestLocation(String provider) {
        mIsLocationChanged = false;
        mCondVar.close();

        // Setups a location listener
        Log.d(Constants.LOGTAG, "provider name: " + provider);
        mRefresherLocationManager.requestLocationUpdates(provider, 0, 0,
                this, mActivity.getApplicationContext().getMainLooper());
        // Sleeps for a while and checks whether location has been changed
        mCondVar.block(mTimeOutSec * 1000);
        // Unregisters the location listener, and then shows the result
        mRefresherLocationManager.removeUpdates(this);
        return mIsLocationChanged;
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }
}

package com.mtime.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.ImageView;

public class AsyncImageLoader {
	private final static String TAG = "AsyncImage";

	public static Drawable loadDrawable(final String imageUrl,
			final ImageCallback imageCallback, final Activity activity) {
		try {
			// System.out.println(imageUrl);
			Drawable drawable = loadImageFromCache(imageUrl);
			if (drawable != null) {
				Log.d(TAG, "Cache Hit:" + imageUrl);
				return drawable;
			}
			Log.d(TAG, "Cache Miss:" + imageUrl);

			final Handler handler = new Handler() {
				@Override
				public void handleMessage(Message message) {
					if (imageCallback != null)
						imageCallback.imageLoaded((Drawable) message.obj,
								imageUrl);
				}
			};
			new Thread() {
				@Override
				public void run() {
					Drawable drawable = loadImageFromUrl(imageUrl, activity);
					Log.d(TAG, "URL Hit:" + imageUrl);
					Message message = handler.obtainMessage(0, drawable);
					handler.sendMessage(message);
				}
			}.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	public static Drawable loadDrawable(String url, Activity activity) {
		Drawable d = loadImageFromCache(url);
		if (d == null) {
			d = loadImageFromUrl(url, activity);
		}
		return d;
	}

	public static Drawable loadImageFromCache(String url) {

		InputStream inputStream = FileLocalCache.load(url);
		if (inputStream != null) {
			return Drawable.createFromStream(inputStream, "src");
		} else {
			return null;
		}
	}

	public static Drawable loadImageFromUrl(String url, Activity activity) {
		InputStream inputStream = null;

		try {
			inputStream = new URL(url).openStream();
		} catch (IOException e) {
			MtimeUtils.gotoErrorActivity(activity);
		}

		inputStream = FileLocalCache.save(url, inputStream);

		return Drawable.createFromStream(inputStream, "src");
	}

	public interface ImageCallback {
		public void imageLoaded(Drawable imageDrawable, String imageUrl);
	}

	public static class DefaultImageCallback implements ImageCallback {
		private ImageView v;

		public DefaultImageCallback(ImageView v) {
			this.v = v;
		}

		@Override
		public void imageLoaded(Drawable imageDrawable, String imageUrl) {
			Log.i(TAG, imageUrl);
			v.setImageDrawable(imageDrawable);

		}
	}
}

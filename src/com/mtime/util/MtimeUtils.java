package com.mtime.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.cookie.Cookie;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.Toast;

import com.mtime.Constants;
import com.mtime.MovieReminderAlarm;
import com.mtime.MovieSessionSelectedActivity;
import com.mtime.NetworkErrorActivity;
import com.mtime.R;
import com.mtime.data.Cinema;
import com.mtime.data.Locations;

public class MtimeUtils {
    /**
     * 截短字符串，如果字符串大于给定长度，则截断，然后后面补充"..."
     * 
     * @param s
     * @param l
     * @return
     */
    public static String tinyString(String s, int l) {
        if (s.length() > l) {
            s = s.substring(0, l) + "...";
        }
        return s;
    }

    /**
     * 格式化打分
     * 
     * @param r
     * @return 返回字符串数组，长度为2，比如说9.23，那么返回值，[0]=9,[1]=.2
     */
    public static String[] formatRatingScore(double r) {
        String[] result = new String[2];
        String s = String.valueOf(r);
        int l = s.indexOf('.');
        if (l > 0) {
            result[0] = s.substring(0, l);
            result[1] = s.substring(l, l + 2);
        } else {
            result[0] = s;
            result[1] = ".0";
        }
        return result;
    }

    private static SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm");
    private static SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");
    private static SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd");

    public static String getTime(long d) {
        return sdf1.format(new Date(d));
    }

    public static String getSimpleDate(long d) {
        if (d == 0l)
            return "";
        return sdf3.format(new Date(d));
    }

    /**
     * 返回从今天起，指定数量的日期
     * 
     * @param n
     * @return
     */
    public static String[] getDays(int n) {
        Calendar c = Calendar.getInstance();
        String[] result = new String[n];
        for (int i = 0; i < n; i++) {
            result[i] = sdf2.format(c.getTime());
            c.add(Calendar.DAY_OF_YEAR, 1);
        }
        return result;
    }

    public static String getDayDesc(String d) {
        return d.substring(4, 6).trim() + "月" + d.substring(6, 8).trim() + "日";
    }

    /**
     * 得到已经设定的当前位置
     * 
     * @param context
     * @return
     */
    public static Locations getLocation(Context context) {

        Locations l = new Locations();

        SharedPreferences preferences = context.getSharedPreferences(
                Constants.LOGTAG, Context.MODE_PRIVATE);
        String locationName = preferences.getString(Constants.CITY, context
                .getString(R.string.default_city));
        int locationId = preferences.getInt(Constants.CITY_CODE, Integer
                .parseInt(context.getString(R.string.default_city_id)));

        l.setId(locationId);
        l.setName(locationName);
        return l;
    }

    /**
     * 保存当前位置
     * 
     * @param context
     * @param location
     */
    public static void setLocations(Context context, Locations location) {
        SharedPreferences.Editor editor = context.getSharedPreferences(
                Constants.LOGTAG, Context.MODE_PRIVATE).edit();
        editor.putString(Constants.CITY, location.getName());
        editor.putInt(Constants.CITY_CODE, location.getId());
        editor.commit();
    }

    public static String getAdsImage(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(
                Constants.LOGTAG, Context.MODE_PRIVATE);
        String adsImageUrl = preferences.getString(Constants.ADS_IMAGE, null);
        return adsImageUrl;
    }

    public static void setAdsImage(Context context, String adsImageUrl) {
        SharedPreferences.Editor editor = context.getSharedPreferences(
                Constants.LOGTAG, Context.MODE_PRIVATE).edit();
        editor.putString(Constants.ADS_IMAGE, adsImageUrl);
        editor.commit();
    }

    /**
     * 得到已经登录的email/password
     * 
     * @param context
     * @return
     */
    public static String[] getLoginUser(Context context) {

        String[] l = new String[2];

        SharedPreferences preferences = context.getSharedPreferences(
                Constants.LOGTAG, Context.MODE_PRIVATE);
        String email = preferences.getString(Constants.EMAIL, "");
        String password = preferences.getString(Constants.PASSWORD, "");
        l[0] = email;
        l[1] = password;
        return l;
    }

    /**
     * 保存登录的email/password
     * 
     * @param context
     * @param location
     */
    public static void setLoginUser(Context context, String email,
            String password) {
        SharedPreferences.Editor editor = context.getSharedPreferences(
                Constants.LOGTAG, Context.MODE_PRIVATE).edit();
        editor.putString(Constants.EMAIL, email);
        editor.putString(Constants.PASSWORD, password);
        editor.commit();
    }

    /**
     * 设置默认影院
     * 
     * @param context
     * @param cinema
     */
    public static void setDefaultCinema(Context context, Cinema cinema) {
        SharedPreferences.Editor editor = context.getSharedPreferences(
                Constants.LOGTAG, Context.MODE_PRIVATE).edit();
        editor.putInt(Constants.DEFAULT_CINEMA_ID, cinema.getId());
        editor.putString(Constants.DEFAULT_CINEMA_NAME, cinema.getName());
        editor.putInt(Constants.DEFAULT_LOCATION_ID, cinema.getLocationId());
        editor.putString(Constants.DEFAULT_LOCATION_NAME, cinema
                .getLocationName());
        editor.commit();
    }

    /**
     * 得到已经设置过的默认影院
     * 
     * @param context
     * @return
     */
    public static Cinema getDefaultCinema(Context context) {
        Cinema cinema = new Cinema();
        SharedPreferences preferences = context.getSharedPreferences(
                Constants.LOGTAG, Context.MODE_PRIVATE);
        cinema.setId(preferences.getInt(Constants.DEFAULT_CINEMA_ID, 0));
        cinema
                .setName(preferences.getString(Constants.DEFAULT_CINEMA_NAME,
                        ""));
        cinema.setLocationId(preferences.getInt(Constants.DEFAULT_LOCATION_ID,
                0));
        cinema.setLocationName(preferences.getString(
                Constants.DEFAULT_LOCATION_NAME, ""));
        return cinema;
    }

    /**
     * Shows toast message with short duration.
     * 
     * @param activity
     *            application activity
     * @param message
     *            message to show
     */
    public static void showShortToastMessage(Activity activity,
            final String message) {
        showToastMessage(activity, message, Toast.LENGTH_SHORT);
    }

    private static void showToastMessage(final Activity activity,
            final String message, final int duration) {
        if (TextUtils.isEmpty(message))
            return;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity.getApplicationContext(), message,
                        duration).show();
            }
        });
    }

    public static void printErrorStackTrace(Exception ex) {
        FileLocalCache.printErrorStackTrace(ex);
    }

    /**
     * 设置默认属性
     * 
     * @param context
     * @param cinema
     */
    public static void setNotifySetting(Context context, String key,
            boolean value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(
                Constants.LOGTAG, Context.MODE_PRIVATE).edit();

        editor.putBoolean(key, value);
        editor.commit();
    }

    /**
     * 得到已经设置过的属性
     * 
     * @param context
     * @return
     */
    public static boolean getNotifySetting(Context context, String key) {
        SharedPreferences preferences = context.getSharedPreferences(
                Constants.LOGTAG, Context.MODE_PRIVATE);
        return preferences.getBoolean(key, false);
    }

    @SuppressWarnings("unchecked")
    public static List<Cookie> getCookies() {
        List<Cookie> cookies = null;
        try {
            File d = new File(Constants.COOKIES_STORE_PATH);
            if (!d.exists())
                d.mkdirs();
            File f = new File(Constants.COOKIES_STORE_PATH + "/"
                    + FileLocalCache.md5(Constants.COOKIES));
            if (!f.exists())
                f.createNewFile();
            if (f.length() == 0)
                return null;

            FileInputStream byteOut = new FileInputStream(f);
            ObjectInputStream out = new ObjectInputStream(byteOut);
            cookies = (List<Cookie>) out.readObject();
            out.close();
            byteOut.close();
        } catch (StreamCorruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (OptionalDataException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return cookies;
    }

    public static void setCookies(List<Cookie> cookies) {
        try {
            FileOutputStream bytetOut = new FileOutputStream(new File(
                    Constants.COOKIES_STORE_PATH + "/"
                            + FileLocalCache.md5(Constants.COOKIES)));
            ObjectOutputStream outer = new ObjectOutputStream(bytetOut);
            outer.writeObject(cookies);
            outer.flush();
            outer.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void clearCookies() {
        try {
            File f = new File(Constants.COOKIES_STORE_PATH + "/"
                    + FileLocalCache.md5(Constants.COOKIES));
            if (f.exists())
                f.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void gotoErrorActivity(Activity activity) {
        Intent i = new Intent();
        i.setClass(activity, NetworkErrorActivity.class);
        activity.startActivity(i);
    }

    public static void cancelEventInBroadcast(Context context, Class clazz,
            int intentNumber) {
        // Cancel this alarm in alarm manager.
        AlarmManager am = (AlarmManager) context
                .getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent();
        intent.setClass(context, clazz);
        intent.setData(Uri.parse(Constants.MTIME_ALARM + intentNumber));
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent,
                PendingIntent.FLAG_NO_CREATE);
        if (null != sender){
            am.cancel(sender);
            sender.cancel();
        }
    }
    
    public static void cancelEventInActivity(Context context, Class clazz,
            int intentNumber) {
        // Cancel this pending event.
        Intent intent = new Intent();
        intent.setClass(context, clazz);
        intent.setData(Uri.parse(Constants.MTIME_ALARM + intentNumber));
        PendingIntent sender = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_NO_CREATE);
        if (null != sender){
            sender.cancel();
        }
    }
}

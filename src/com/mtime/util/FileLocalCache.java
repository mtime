package com.mtime.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;

import com.mtime.Constants;

public class FileLocalCache {
	private static final String cacheDir = Constants.CACHE_STORE_PATH;

	public static boolean checkDir() {
		File f = new File(cacheDir);
		if (!f.exists()) {
			return f.mkdirs();
		}
		return true;
	}

	public static InputStream load(String url) {
		if (checkDir()) {

			String md5 = md5(url);
			File f = new File(cacheDir + md5);
			if (f.exists()) {
				try {
					return new FileInputStream(f);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public static InputStream save(String url, InputStream in) {
		if (checkDir()) {
			String md5 = md5(url);
			try {
				File f = new File(cacheDir + md5);
				if (!f.exists()) {
					f.createNewFile();
				}
				FileOutputStream out = new FileOutputStream(f);

				int b;
				do {
					b = in.read();
					if (b != -1) {
						out.write(b);
					}
				} while (b != -1);
				in.close();
				out.flush();
				out.close();
				return new FileInputStream(f);

			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		} else {
			return in;
		}

	}

	public static String md5(String url) {
		try {
			MessageDigest md5 = java.security.MessageDigest.getInstance("MD5");
			md5.update(url.getBytes("UTF-8"));
			byte messageDigest[] = md5.digest();

			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++) {
				String t = Integer.toHexString(0xFF & messageDigest[i]);
				if (t.length() == 1) {
					hexString.append("0" + t);
				} else {
					hexString.append(t);
				}
			}
			return hexString.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void printErrorStackTrace(Exception ex) {
		if (FileLocalCache.checkDir()) {

			File f = new File("/sdcard/Mtime/error.log");

			try {
				if (!f.exists())
					f.createNewFile();
				ex.printStackTrace(new PrintStream(f));
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	public static void clearCache() {
		File f = new File(cacheDir);
		if (f.exists() && f.isDirectory()) {
			File flist[] = f.listFiles();
			for (int i = 0; flist != null && i < flist.length; i++) {
				flist[i].delete();
			}

		}
	}

	public static String getCacheSize() {
		long size = 0;
		File f = new File(cacheDir);
		if (f.exists() && f.isDirectory()) {
			File flist[] = f.listFiles();
			for (int i = 0; flist != null && i < flist.length; i++) {
				size = size + flist[i].length();
			}

		}
		if (size < 1000) {
			return "0k";
		} else if (size < 1000000) {
			return size / 1000 + "k";
		} else {
			return size / 1000000 + "m";
		}
	}

	public static String load2(String url) {
		String md5 = md5(url);
		File f = new File(cacheDir + md5);
		// Cache in an hour
		long expiredTime = 3600000l;

		if (f.exists()
				&& System.currentTimeMillis() - f.lastModified() < expiredTime) {

			try {
				FileInputStream fstream = new FileInputStream(f);
				long length = f.length();

				byte[] bytes = new byte[(int) length];

				// Read in the bytes
				int offset = 0;
				int numRead = 0;
				while (offset < bytes.length
						&& (numRead = fstream.read(bytes, offset, bytes.length
								- offset)) >= 0) {
					offset += numRead;
				}

				return new String(bytes, "UTF-8");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static void store(String url, String c) {
		String md5 = md5(url);
		File f = new File(cacheDir + md5);

		try {
			FileOutputStream out;
			out = new FileOutputStream(f);
			out.write(c.getBytes("UTF-8"));
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}

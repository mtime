package com.mtime.util;

import java.util.ArrayList;
import java.util.List;

import com.mtime.data.Movie;

public class MockDataFactory {
	public static List<Movie> getComingMovies() {
		List<Movie> list = getRecentMovies();
		list.addAll(getRecentMovies());
		list.addAll(getRecentMovies());
		return list;
	}

	private static List<Movie> getRecentMovies() {
		List<Movie> list = new ArrayList<Movie>();
		Movie m = null;

		m = new Movie();
		m.setName("十月围城");
		m.setRatingScore(7.3);
		m.setDirectorName("陈德森");
		m.setActorName1("甄子丹");
		m.setActorName2("谢霆锋");
		m.setDuration("120分钟");
		m.setCinemaCount(12);
		m.setShowtimeCount(34);
		m.setShowdate(3242432234l);
		list.add(m);

		m = new Movie();
		m.setName("三枪拍案惊奇");
		m.setRatingScore(4.8);
		m.setDirectorName("张艺谋");
		m.setActorName1("小沈阳");
		m.setActorName2("孙红雷");
		m.setDuration("94分钟");
		m.setShowdate(32242342341l);
		m.setCinemaCount(44);
		m.setShowtimeCount(147);
		list.add(m);

		m = new Movie();
		m.setName("阿凡达");
		m.setRatingScore(9.4);
		m.setDirectorName("詹姆斯·卡梅隆");
		m.setActorName1("萨姆·沃辛顿");
		m.setActorName2("佐伊·索尔达娜");
		m.setDuration("162分钟");
		m.setShowdate(32432432l);
		m.setCinemaCount(53);
		m.setShowtimeCount(887);
		list.add(m);
		return list;
	}

}

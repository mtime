package com.mtime;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.ViewFlipper;

import com.mtime.data.Movie;
import com.mtime.data.RatingResult;
import com.mtime.data.SignInResult;
import com.mtime.util.AsyncImageLoader;
import com.mtime.util.MtimeUtils;

public class RatingActivity extends AbstractMtimeActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.act_welcome);
        // look up the notification manager service
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // cancel the notification that we started in IncomingMessage

        final int movieId = getIntent().getIntExtra(Constants.KEY_MOVIE_ID, -1);

        nm.cancel(R.string.rating_reminder + movieId);
        MtimeUtils.cancelEventInActivity(this, RatingActivity.class,
                getIntent().getIntExtra(Constants.KEY_ALARM_NUMBER, -1));
    }

    @Override
    protected void onStart() {
        super.onStart();

        boolean binding = false;
        SignInResult r = null;
        String[] user = MtimeUtils.getLoginUser(RatingActivity.this);
        if (!"".equals(user[0]) && !"".equals(user[1])) {
            binding = true;

            try {
                // TODO Exception handler
                // Then get nearby cinema's
                r = rs.getSignInResult(user[0], user[1]);
            } catch (Exception e) {

            }
        }

        // if not binding or logged in successfully
        Intent i = new Intent();
        if (!binding || (r != null && r.isSuccess())) {
            showDialog(123);
        } else {
            // loggin failed, clear cookies
            MtimeUtils.clearCookies();
            i.setClass(RatingActivity.this, SettingsActivity.class);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    void doOperation() {
        // TODO Auto-generated method stub
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        Dialog dialog = super.onCreateDialog(id);
        if (dialog != null)
            return dialog;

        LayoutInflater factory = LayoutInflater.from(this);
        final View ratingView = factory.inflate(R.layout.view_rating, null);

        // Override mainBody
        mainBody = ratingView.findViewById(R.id.view_MainBody);
        alertBody = (ViewFlipper) ratingView.findViewById(R.id.view_AlertBody);

        final RatingBar ratingBar = (RatingBar) ratingView
                .findViewById(R.id.rt_RatingBar);
        final EditText ratingText = (EditText) ratingView
                .findViewById(R.id.tv_RatingText);

        ImageView icon = (ImageView) ratingView.findViewById(R.id.icon);

        final int movieId = getIntent().getIntExtra(Constants.KEY_MOVIE_ID, -1);
        // final int movieId = 91865;
        Movie movie = rs.getMovieDetail(movieId);
        String movieSrc = movie.getImageSrc();

        Drawable d = AsyncImageLoader.loadDrawable(movieSrc,
                new AsyncImageLoader.DefaultImageCallback(icon),
                RatingActivity.this);

        if (d == null)
            d = RatingActivity.this.getResources().getDrawable(
                    R.drawable.default_movie_post);
        icon.setImageDrawable(d);

        dialog = new AlertDialog.Builder(this).setView(ratingView).setTitle(
                getString(R.string.do_you_like_this_film)).setPositiveButton(
                R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                        new Task() {
                            RatingResult ratingResult;

                            @Override
                            public void after() {
                                // if (ratingResult.getError() == null
                                // || ratingResult.getError().length()
                                // == 0) {
                                Intent i = new Intent();
                                i.setClass(RatingActivity.this,
                                        MovieRecentListActivity.class);
                                RatingActivity.this.startActivity(i);
                                finish();
                                // }

                            }

                            @Override
                            public void before() throws Exception {
                                int ratingScore = ratingBar.getProgress();
                                int inpressRatingScore = 0, storyRatingScore = 0, showRatingScore = 0, directorRatingScore = 0, screenRatingScore = 0, musicRatingScore = 0;
                                String comment = ratingText.getText()
                                        .toString();
                                ratingResult = rs.getRatingMovie(movieId,
                                        ratingScore, inpressRatingScore,
                                        storyRatingScore, showRatingScore,
                                        directorRatingScore, screenRatingScore,
                                        musicRatingScore, comment);
                            }
                        }.start();
                    }
                }).setNegativeButton(R.string.cancel,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent();
                        i.setClass(RatingActivity.this,
                                MovieRecentListActivity.class);
                        RatingActivity.this.startActivity(i);
                        finish();

                    }

                }).create();

        dialog.setCancelable(false);
        return dialog;
    }
}
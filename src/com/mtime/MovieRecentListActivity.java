package com.mtime;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;

import com.mtime.Constants.ViewClass;
import com.mtime.data.Cinema;
import com.mtime.data.Movie;
import com.mtime.util.AsyncImageLoader;
import com.mtime.util.MtimeUtils;

public class MovieRecentListActivity extends AbstractMovieListActivity {

	private int cinemaId = 0;
	private String cinemaName = "";
	private Button locationMoviesBtn;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_movie_recent_list);
	}

	private void showRecentMovieView(final int locationId,
			final String locationName, int cinemaId, String cinemaName) {
		if (cinemaId == 0) {
			showLocationRecentMovieView(locationId);
		} else {
			showCinemaRecentMovieView(locationId, locationName, cinemaId,
					cinemaName);
		}

	}

	private void showCinemaRecentMovieView(final int locationId,
			final String locationName, final int cinemaId,
			final String cinemaName) {

		new Task() {
			List<Movie> movieList;

			@Override
			public void after() {

				showCinemaRecentMovieView(locationId, locationName, cinemaId,
						cinemaName, movieList);
			}

			@Override
			public void before() throws Exception {
				movieList = rs.getCinemaMovies(locationId, cinemaId);
			}
		}.start();
	}

	private void showCinemaRecentMovieView(final int locationId,
			final String locationName, int cinemaId, String cinemaName,
			final List<Movie> movieList) {
		navTitleView.setText(MtimeUtils.tinyString(cinemaName, 9));
		locationMoviesBtn.setVisibility(View.VISIBLE);

		if (movieList.size() == 0) {
			emptyAlert.setText(R.string.no_movie_in_this_cinema);
			emptyAlert.setVisibility(View.VISIBLE);
			return;
		} else {
			emptyAlert.setVisibility(View.GONE);
		}

		showRecentMovieView(movieList);

	}

	private void showLocationRecentMovieView(final int locationId) {
		Log.d(Constants.LOGTAG, "showLocationRecentMovieView");

		new Task() {
			List<Movie> movieList;

			@Override
			public void after() {
				if (null == locations)
					MtimeUtils.gotoErrorActivity(MovieRecentListActivity.this);
				else
					showLocationRecentMovieView(locationId, movieList);

			}

			@Override
			public void before() throws Exception {
				locations = rs.getChinaLocations();
				movieList = rs.getLocationMovies(locationId);
			}
		}.start();

	}

	private void showLocationRecentMovieView(int locationId,
			List<Movie> movieList) {
		locationMoviesBtn.setVisibility(View.GONE);
		navTitleView.setText(R.string.welcome);

		if (movieList.size() == 0) {
			emptyAlert.setText(R.string.no_movie_in_this_city);
			emptyAlert.setVisibility(View.VISIBLE);
			return;
		} else {
			emptyAlert.setVisibility(View.GONE);
		}

		showRecentMovieView(movieList);

	}

	private void showRecentMovieView(final List<Movie> movieList) {

		final TextView movieName, ratingScore1, ratingScore2, moviePeoples;

		movieName = (TextView) findViewById(R.id.tv_MovieName);
		ratingScore1 = (TextView) findViewById(R.id.tv_RatingScore1);
		ratingScore2 = (TextView) findViewById(R.id.tv_RatingScore2);
		moviePeoples = (TextView) findViewById(R.id.tv_MoviePeoples);
		Gallery gallery = (Gallery) findViewById(R.id.gallery);

		gallery.setAdapter(new MyAdapter(movieList));
		if (movieList.size() > 0)
			gallery.setSelection(1);

		gallery.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent i = new Intent();
				if (cinemaId == 0)
					i.setClass(MovieRecentListActivity.this,
							MovieCinemaListActivity.class);
				else
					i.setClass(MovieRecentListActivity.this,
							MovieSessionListActivity.class);

				i.putExtra(Constants.KEY_MOVIE_ID, movieList.get(position)
						.getId());
				i.putExtra(Constants.KEY_MOVIE_NAME, movieList.get(position)
						.getName());

				i.putExtra(Constants.KEY_CINEMA_ID, cinemaId);
				i.putExtra(Constants.KEY_CINEMA_NAME, cinemaName);

				MovieRecentListActivity.this.startActivity(i);
			}

		});
		gallery.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				Movie m = movieList.get(position);
				movieName.setText(m.getName());
				if (m.getRatingScore() > 0) {
					ratingScore1.setText(String.valueOf(m.getRatingScore())
							.substring(0, 1));
					ratingScore2.setText(String.valueOf(m.getRatingScore())
							.substring(1, 3));
				}
				String peoples = getString(R.string.director) + " "
						+ m.getDirectorName() + "\n"
						+ getString(R.string.main_actor) + " "
						+ m.getActorName1() + " " + m.getActorName2() + "\n"
						+ getString(R.string.duration) + " " + m.getDuration()
						+ "\n" + m.getCinemaCount()
						+ getString(R.string.cinema_show)
						+ m.getShowtimeCount() + getString(R.string.shows);

				moviePeoples.setText(peoples);

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
	}

	class MyAdapter extends BaseAdapter {
		int mGalleryItemBackground;
		List<Movie> movieList;
		List<View> galleryViewList;

		public MyAdapter(List<Movie> movieList) {
			TypedArray a = obtainStyledAttributes(R.styleable.Gallery1);
			mGalleryItemBackground = a.getResourceId(
					R.styleable.Gallery1_android_galleryItemBackground, 0);
			a.recycle();
			this.movieList = movieList;
			this.galleryViewList = new ArrayList<View>(movieList.size());
			for (int i = 0; i < movieList.size(); i++)
				galleryViewList.add(null);
			// galleryViewList.add(buildView(movieList.get(i)));
		}

		private View buildView(Movie movie) {
			final ImageView v = new ImageView(MovieRecentListActivity.this);

			String imageUrl = movie.getImageSrc();

			Drawable d = AsyncImageLoader.loadDrawable(imageUrl,
					new AsyncImageLoader.DefaultImageCallback(v),
					MovieRecentListActivity.this);

			if (d == null)
				d = MovieRecentListActivity.this.getResources().getDrawable(
						R.drawable.default_movie_post);
			v.setImageDrawable(d);
			v.setScaleType(ImageView.ScaleType.FIT_XY);
			v.setLayoutParams(new Gallery.LayoutParams(150, 200));

			// The preferred Gallery item background
			v.setBackgroundResource(mGalleryItemBackground);
			return v;
		}

		@Override
		public int getCount() {
			return movieList.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {

			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = galleryViewList.get(position);
			if (v == null) {
				v = buildView(movieList.get(position));
				galleryViewList.set(position, v);

			}
			return v;

		}
	}

	@Override
	void doOperation() {
		currentView = ViewClass.MovieRecentList;
		locationMoviesBtn = (Button) findViewById(R.id.btn_LocationMovies);
		locationMoviesBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showLocationRecentMovieView(currentLocation.getId());
			}

		});

		int cinemaId = getIntent().getIntExtra(Constants.KEY_CINEMA_ID, -1);

		String cinemaName = getIntent().getStringExtra(
				Constants.KEY_CINEMA_NAME);

		Cinema defaultCinema = MtimeUtils.getDefaultCinema(this);
		if (defaultCinema != null && defaultCinema.getId() > 0) {
			this.cinemaId = defaultCinema.getId();
			this.cinemaName = defaultCinema.getName();
		}

		if (cinemaId == -1) {
			if (defaultCinema != null && defaultCinema.getId() > 0) {
				Log.d("MT", "location=" + defaultCinema.getLocationId());
				showRecentMovieView(defaultCinema.getLocationId(),
						defaultCinema.getLocationName(), this.cinemaId,
						this.cinemaName);
			} else {
				Log.d("MT3", "location=" + currentLocation.getId());
				showRecentMovieView(currentLocation.getId(), currentLocation
						.getName(), 0, "");
			}

		} else {
			this.cinemaId = cinemaId;
			this.cinemaName = cinemaName;
			Log.d("MT2", "location=" + currentLocation.getId());
			showRecentMovieView(currentLocation.getId(), currentLocation
					.getName(), cinemaId, cinemaName);

		}
	}
}
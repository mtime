package com.mtime;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;

/*
 * Utility functions collection for MapView.
 */
public class PoiMapViewUtil {
	private PoiMapViewUtil() {
	}

	@SuppressWarnings("unchecked")
	private static class MyItemizedOverlay extends ItemizedOverlay {
		private final ArrayList<OverlayItem> mOverlays = new ArrayList<OverlayItem>();

		public MyItemizedOverlay(Drawable defaultMarker, boolean centerAlign) {
			super(centerAlign ? boundCenter(defaultMarker)
					: boundCenterBottom(defaultMarker));
		}

		@Override
		protected OverlayItem createItem(int i) {
			return mOverlays.get(i);
		}

		@Override
		public int size() {
			return mOverlays.size();
		}

		public void addOverlay(OverlayItem overlay) {
			mOverlays.add(overlay);
			populate();
		}

		@Override
		public void draw(Canvas canvas, MapView mapView, boolean shadow) {
			super.draw(canvas, mapView, false);
		}
	}

	/**
	 * Shows a POI on a MapView and keeps the zoom level.
	 * 
	 * @param activity
	 *            the activity
	 * @param mapView
	 *            the MapView to show the POI
	 * @param mapPoi
	 *            the POI
	 * @param mapCenter
	 *            true to center the POI on map
	 * @return a {@link PoiOnMapView} object, which can be removed by calling
	 *         {@link #removePoiFromMapView}
	 */
	public static PoiOnMapView showMapPoi(Activity activity, MapView mapView,
			MapPoi mapPoi, boolean mapCenter) {
		return showMapPoi(activity, mapView, mapPoi, mapCenter, 0);
	}

	/**
	 * Shows a POI on a MapView and set the zoom level to a specific value.
	 * 
	 * @param activity
	 *            the activity
	 * @param mapView
	 *            the MapView to show the POI
	 * @param mapPoi
	 *            the POI
	 * @param mapCenter
	 *            true to center the POI on map
	 * @param zoomLevel
	 *            the zoom level
	 * @return a {@link PoiOnMapView} object, which can be removed by calling
	 *         {@link #removePoiFromMapView}
	 */
	public static PoiOnMapView showMapPoi(Activity activity, MapView mapView,
			MapPoi mapPoi, boolean mapCenter, int zoomLevel) {
		double lat = mapPoi.getLatitude();
		double lng = mapPoi.getLongitude();

		GeoPoint p = new GeoPoint((int) (lat * Constants.E6),
				(int) (lng * Constants.E6));

		MyItemizedOverlay overlay = new MyItemizedOverlay(mapPoi
				.getDrawable(activity), mapPoi.isCenterAlign());

		// Sets zoom level and centers this point if necessary
		if (zoomLevel > 0)
			mapView.getController().setZoom(zoomLevel);
		if (mapCenter)
			mapView.getController().setCenter(p);

		// Adds this point to map overlays
		OverlayItem item = new OverlayItem(p, "", "");
		overlay.addOverlay(item);
		mapView.getOverlays().add(overlay);

		// Adds popup window
		final View popupView = activity.getLayoutInflater().inflate(
				R.layout.map_popup, null);
		mapView.addView(popupView);

		// Hides the popup by default
		popupView.setVisibility(View.GONE);

		// Sets up onClick listener on the popup
		hookOnClickListener(popupView, mapPoi.getPopupOnClickListener());
		// Prepares the layout parameter
		MapView.LayoutParams param = new MapView.LayoutParams(
				MapView.LayoutParams.WRAP_CONTENT,
				MapView.LayoutParams.WRAP_CONTENT, p, 0, mapPoi
						.getPopupYShiftPixel(),
				MapView.LayoutParams.BOTTOM_CENTER);

		CharSequence title = mapPoi.getTitle();
		if (!TextUtils.isEmpty(title)) {
			// Adds title
			TextView titleView = (TextView) popupView
					.findViewById(R.id.popup_title);
			titleView.setText(title);

			// Adds snippet
			TextView snippetView = (TextView) popupView
					.findViewById(R.id.popup_snippet);
			CharSequence snippet = mapPoi.getSnippet();
			if (!TextUtils.isEmpty(snippet)) {
				snippetView.setText(snippet);
				if (mapPoi.isReverseGeocodingNeeded()) {
					updateSnippetToAddressInBackground(activity, lat, lng,
							snippetView);
				}
				snippetView.setVisibility(View.VISIBLE);
			} else {
				snippetView.setVisibility(View.GONE);
			}

			// Adds image
			if (mapPoi.hasImage()) {
				popupView.findViewById(R.id.popup_image_plate).setVisibility(
						View.VISIBLE);
				((ImageView) popupView.findViewById(R.id.popup_image))
						.setImageBitmap(mapPoi.getImage());
			} else {
				popupView.findViewById(R.id.popup_image_plate).setVisibility(
						View.GONE);
			}

			// Hook onClick listener on close icon for closing the popup
			View closeIcon = popupView.findViewById(R.id.popup_close);
			if (mapPoi.isClosable()) {
				closeIcon.setVisibility(View.VISIBLE);
				closeIcon.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						popupView.setVisibility(View.GONE);
					}
				});
			} else {
				closeIcon.setVisibility(View.GONE);
			}
		}

		return new PoiOnMapView(mapView, mapPoi, popupView, param, (mapView
				.getOverlays().size() - 1));
	}

	private static void hookOnClickListener(final View popupView,
			final OnClickListener listener) {
		if (listener == null)
			return;

		View plateView = popupView.findViewById(R.id.popup_plate);
		plateView.setOnClickListener(listener);
		plateView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					popupView.setSelected(true);
					// Calls onClick to make it work on Donut (SDK r1)
					if (Integer.parseInt(Build.VERSION.SDK) >= 4) {
						listener.onClick(v);
					}
					break;
				case MotionEvent.ACTION_UP: // Falls through
				case MotionEvent.ACTION_CANCEL:
					popupView.setSelected(false);
					break;
				}
				return false;
			}
		});
	}

	/**
	 * Removes a POI from MapView.
	 * 
	 * @param poi
	 *            the POI to be removed
	 */
	public static void removePoiFromMapView(PoiOnMapView poi) {
		if (poi == null)
			return;

		MapView mapView = poi.getMapView();
		mapView.removeView(poi.getPopupView());
		mapView.getOverlays().remove(poi.getOverlayIndex());
	}

	private static void updateSnippetToAddressInBackground(
			final Activity activity, final double lat, final double lng,
			final TextView snippetView) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				final String addr = getAddressFromLatLng(activity, lat, lng);
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						snippetView
								.setText(TextUtils.isEmpty(addr) ? "There is now such address."
										: addr);
					}
				});
			}
		}).start();
	}

	/**
	 * Gets address from a lat/lng.
	 * 
	 * @param context
	 *            context
	 * @param lat
	 *            latitude
	 * @param lng
	 *            longitude
	 * @return empty string if cannot get the address
	 */
	public static String getAddressFromLatLng(Context context, double lat,
			double lng) {
		try {
			List<Address> address = new Geocoder(context, Locale.getDefault())
					.getFromLocation(lat, lng, 1);

			if (address.size() > 0) {
				// TODO: use feature name (address.get(0).getFeatureName()) if
				// Geocoder is able to return feature name
				String locality = address.get(0).getLocality();
				if (!TextUtils.isEmpty(locality)) {
					String thoroughfare = address.get(0).getThoroughfare();
					return TextUtils.isEmpty(thoroughfare) ? locality
							: (thoroughfare + ", " + locality);
				} else {
					// If there is no locality and thoroughfare, just return
					// the address line
					// NOTE: known issue: the getAddressLine() will return
					// country name instead of detailed address for JP lat/lng
					String addressLine = address.get(0).getAddressLine(0);
					if (!TextUtils.isEmpty(addressLine)) {
						return addressLine;
					}
				}
			} else {
				Log.d(Constants.LOGTAG, "unable to get address from location: "
						+ lat + ", " + lng);
			}
		} catch (IOException e) {
			Log.d(Constants.LOGTAG, "unable to get address from location: "
					+ lat + ", " + lng);
		}

		// No address is available
		return "";
	}

	@SuppressWarnings("unused")
	private static void dumpAddressList(Context context, double lat, double lng)
			throws IOException {
		List<Address> addressList = new Geocoder(context, Locale.getDefault())
				.getFromLocation(lat, lng, 5);
		for (int i = 0; i < addressList.size(); i++) {
			Address address = addressList.get(i);
			Log.d(Constants.LOGTAG, "=== address list " + i + " ====");
			for (int j = 0; j <= address.getMaxAddressLineIndex(); j++) {
				Log.e(Constants.LOGTAG, "address line " + j + ": "
						+ address.getAddressLine(j));
			}
			Log.d(Constants.LOGTAG, "phone: " + address.getPhone());
			Log
					.d(Constants.LOGTAG, "feature name: "
							+ address.getFeatureName());
			Log.d(Constants.LOGTAG, "thoroughfare: "
					+ address.getThoroughfare());
			Log.d(Constants.LOGTAG, "locality: " + address.getLocality());
			Log.d(Constants.LOGTAG, "sub-admin area name: "
					+ address.getSubAdminArea());
			Log.d(Constants.LOGTAG, "admin area name: "
					+ address.getAdminArea());
			Log.d(Constants.LOGTAG, "postal code: " + address.getPostalCode());
			Log
					.d(Constants.LOGTAG, "country name: "
							+ address.getCountryName());
			Log.d(Constants.LOGTAG, "toString: " + address.toString());
		}
	}

	/**
	 * Detects a double tapping on MapView.
	 * 
	 * @param mapView
	 *            the MapView
	 * @param tapTime1
	 *            the time-stamp of the first tapping
	 * @param tapTime2
	 *            the time-stamp of the second tapping
	 * @param point1
	 *            the screen point of the first tapping
	 * @param point2
	 *            the screen point of the second tapping
	 * @param geoPoint1
	 *            the Geo point of the first tapping
	 * @param geoPoint2
	 *            the Geo point of the second tapping
	 * @return true if it's a double tapping
	 */
	public static boolean isDoubleTap(MapView mapView, long tapTime1,
			long tapTime2, Point point1, Point point2, GeoPoint geoPoint1,
			GeoPoint geoPoint2) {

		// Gets the pixel distance between two touch points
		float[] results = new float[2];
		Location.distanceBetween(getLatitudeFromGeoPoint(geoPoint1),
				getLongitudeFromGeoPoint(geoPoint1),
				getLatitudeFromGeoPoint(geoPoint2),
				getLongitudeFromGeoPoint(geoPoint2), results);
		float pixelDistance = mapView.getProjection().metersToEquatorPixels(
				results[0]);

		return ((pixelDistance < 10) && (tapTime2 - tapTime1) < 500)
				&& (Math.abs(point1.x - point2.x) < 50)
				&& (Math.abs(point1.y - point2.y) < 50);
	}

	/**
	 * Gets the latitude from a Geo point.
	 */
	public static double getLatitudeFromGeoPoint(GeoPoint p) {
		return (double) p.getLatitudeE6() / Constants.E6;
	}

	/**
	 * Gets the longitude from a Geo point.
	 */
	public static double getLongitudeFromGeoPoint(GeoPoint p) {
		return (double) p.getLongitudeE6() / Constants.E6;
	}

	public static int getDistance(double lat1, double lng1, double lat2,
			double lng2) {
		float[] results = new float[2];
		Location.distanceBetween(lat1, lng1, lat2, lng2, results);
		return (int) results[0];
	}
}
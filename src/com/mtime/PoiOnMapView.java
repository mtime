package com.mtime;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.view.View;
import android.widget.ImageView;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Projection;

/**
 * This class stores the information of a POI on MapView.
 */
public class PoiOnMapView {
    private MapView mMapView;
    private MapPoi mMapPoi;
    private GeoPoint mPoint;
    private View mPopupView;
    private int mOverlayIndex;
    private MapView.LayoutParams mLayoutParam;

    /**
     * Constructs a {@link PoiOnMapView} object
     *
     * @param mapView the MapView which POI is displayed on
     * @param mapPoi the POI
     * @param popupView the view of the popup window
     * @param layoutParam the layout param of popup view
     * @param overlayIndex the index in MapView overlays
     */
    public PoiOnMapView(MapView mapView, MapPoi mapPoi, View popupView,
            MapView.LayoutParams layoutParam, int overlayIndex) {
        mMapView = mapView;
        mMapPoi = mapPoi;
        mPoint = new GeoPoint((int) (mapPoi.getLatitude() * Constants.E6),
                (int) (mapPoi.getLongitude() * Constants.E6));
        mPopupView = popupView;
        mLayoutParam = layoutParam;
        mOverlayIndex = overlayIndex;
    }

    public MapView getMapView() {
        return mMapView;
    }

    public View getPopupView() {
        return mPopupView;
    }

    public int getOverlayIndex() {
        return mOverlayIndex;
    }

    public GeoPoint getPoint() {
        return mPoint;
    }

    /**
     * Updates popup view.
     */
    public void showPopup() {
        // Do not show popup if the title is empty or the popup is already
        // visible
        if ((mMapPoi.hasTitle())
                && (mPopupView.getVisibility() != View.VISIBLE)) {
            mPopupView.setVisibility(View.VISIBLE);
            mMapView.updateViewLayout(mPopupView, mLayoutParam);
        }
    }

    /**
     * Hides popup window.
     */
    public void hidePopup() {
        if (mPopupView.getVisibility() == View.VISIBLE) {
            mPopupView.setVisibility(View.GONE);
            mMapView.updateViewLayout(mPopupView, mLayoutParam);
        }
    }

    /**
     * Centers the POI on map.
     */
    public void centerPoi() {
        centerPoi(0);
    }

    /**
     * Centers the POI on map by shifting Y-pixels
     */
    public void centerPoi(int shiftYPixel) {
        if (shiftYPixel != 0) {
            Projection projection = mMapView.getProjection();
            Point screenPoint = projection.toPixels(mPoint, null);
            GeoPoint flyTo = projection.fromPixels(screenPoint.x,
                    screenPoint.y + shiftYPixel);
            mMapView.getController().animateTo(flyTo);
        } else {
            mMapView.getController().animateTo(mPoint);
        }
    }

    /**
     * Sets the image of popup window.
     */
    public void setPopupImage(Bitmap image) {
        if (image == null) {
            mPopupView.findViewById(R.id.popup_image_plate)
                    .setVisibility(View.GONE);
        } else {
            mPopupView.findViewById(R.id.popup_image_plate)
                    .setVisibility(View.VISIBLE);
            ((ImageView) mPopupView.findViewById(R.id.popup_image))
                    .setImageBitmap(image);
        }
    }
}

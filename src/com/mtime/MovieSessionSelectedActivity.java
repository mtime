package com.mtime;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.apache.http.cookie.Cookie;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.mtime.data.Showtime;
import com.mtime.util.MtimeUtils;

public class MovieSessionSelectedActivity extends AbstractMtimeActivity {

    private Showtime mShowtime;
    private String movieName;
    private String[] weekChinese;
    private String cinemaName;
    private String movieDetails;
    private long deloyMiliSecs;
    private int pressButton;
    private int showtimeId, movieId;
    List<Cookie> loginCookies;
    boolean isLogin;
    boolean isRatingOn;
    boolean isComingOn;
    private static int nowAlarmId = 0;

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case R.id.btn_insert_calenar:
            return new AlertDialog.Builder(MovieSessionSelectedActivity.this)
                    .setTitle(R.string.add_reminder).setPositiveButton(
                            R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int whichButton) {
                                    Intent intent;
                                    PendingIntent sender;
                                    Calendar calendar = Calendar.getInstance();
                                    AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);

                                    //
                                    // Movie reminder
                                    // 
                                    intent = new Intent();
                                    intent.setClass(
                                            MovieSessionSelectedActivity.this,
                                            MovieReminderAlarm.class);
                                    // Set the unique uri
                                    intent.setData(Uri
                                            .parse(Constants.MTIME_ALARM
                                                    + nowAlarmId));
                                    intent.putExtra(Constants.KEY_ALARM_NUMBER,
                                            nowAlarmId);
                                    nowAlarmId++;
                                    intent.putExtra(Constants.KEY_MOVIE_ID,
                                            movieId);
                                    Log
                                            .d(
                                                    "Alarm",
                                                    "MovieReminder0:"
                                                            + (R.string.movie_reminder + movieId)
                                                            + "," + movieId);

                                    intent.putExtra(Constants.KEY_MOVIE_NAME,
                                            movieName);

                                    intent.putExtra(Constants.KEY_CINEMA_NAME,
                                            cinemaName);
                                    intent.putExtra(Constants.KEY_SHOWTIME_ID,
                                            showtimeId);

                                    String t = "半小时";
                                    if (1 == pressButton) {
                                        t = "一小时";
                                    } else if (0 == pressButton) {
                                        t = "半小时";
                                    }
                                    String msg = "将在" + t + "后上映";

                                    intent.putExtra(Constants.KEY_MESSAGE, msg);

                                    sender = PendingIntent.getBroadcast(
                                            MovieSessionSelectedActivity.this,
                                            0, intent, 0);

                                    long alertTime = mShowtime.getTime()
                                            - deloyMiliSecs;
                                    if (alertTime <= 0)
                                        alertTime = System.currentTimeMillis();

                                    calendar.setTimeInMillis(alertTime);
                                    // ******************************
                                    // In test, just remind it after 30
                                    // secs.
                                    if (2 == pressButton) {
                                        calendar.setTimeInMillis(System
                                                .currentTimeMillis());
                                        calendar.add(Calendar.SECOND, 5);
                                    }
                                    // ******************************
                                    am.set(AlarmManager.RTC_WAKEUP, calendar
                                            .getTimeInMillis(), sender);

                                    //                                    
                                    // Rating reminder, if user login and has
                                    // rating notification
                                    //                                    
                                    if (isLogin && isRatingOn) {
                                        // Add it into calendar

                                        intent = new Intent();
                                        intent
                                                .setClass(
                                                        MovieSessionSelectedActivity.this,
                                                        ShowtimeAlarm.class);
                                        // Set the unique uri
                                        intent.setData(Uri
                                                .parse(Constants.MTIME_ALARM
                                                        + nowAlarmId));
                                        intent.putExtra(
                                                Constants.KEY_ALARM_NUMBER,
                                                nowAlarmId);
                                        nowAlarmId++;

                                        intent.putExtra(Constants.MOVIE_DETAIL,
                                                movieDetails);
                                        intent.putExtra(Constants.KEY_MOVIE_ID,
                                                movieId);
                                        Log
                                                .d(
                                                        "Alarm",
                                                        "RatingReminder0:"
                                                                + (R.string.rating_reminder + movieId)
                                                                + "," + movieId);
                                        intent.putExtra(
                                                Constants.KEY_MOVIE_NAME,
                                                movieName);
                                        intent.putExtra(Constants.KEY_MESSAGE,
                                                "好看吗？");
                                        sender = PendingIntent
                                                .getBroadcast(
                                                        MovieSessionSelectedActivity.this,
                                                        0, intent, 0);

                                        long alarmTime = mShowtime.getTime();
                                        int durationMin = mShowtime
                                                .getDuration();
                                        // Default duration is 120 mins.
                                        if (0 == durationMin)
                                            durationMin = 120;
                                        // Will show rating alarm after 30 min
                                        alarmTime += (durationMin + 30) * 60 * 1000;
                                        calendar.setTimeInMillis(alarmTime);

                                        // ******************************
                                        // In test, just remind it after 30
                                        // secs.
                                        if (2 == pressButton) {
                                            calendar.setTimeInMillis(System
                                                    .currentTimeMillis());
                                            calendar.add(Calendar.SECOND, 30);
                                        }
                                        // ******************************

                                        // Schedule the alarm!
                                        am.set(AlarmManager.RTC_WAKEUP,
                                                calendar.getTimeInMillis(),
                                                sender);
                                    }
                                    // Tell users what we have done.
                                    MtimeUtils
                                            .showShortToastMessage(
                                                    MovieSessionSelectedActivity.this,
                                                    getString(R.string.insert_calendar_finish));

                                }
                            }).setNegativeButton(R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int whichButton) {
                                    // Do nothing.
                                }
                            }).setSingleChoiceItems(R.array.reminders, 0,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int whichButton) {
                                    pressButton = whichButton;
                                    Log
                                            .d("SSect", "pressButton="
                                                    + pressButton);
                                    deloyMiliSecs = 30 * (whichButton + 1) * 60 * 1000;
                                }
                            }).create();
        }
        return null;
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.act_movie_session_selected);
        weekChinese = new String[] { "", getString(R.string.sun), getString(R.string.mon),
                getString(R.string.tues), getString(R.string.wen),
                getString(R.string.thur), getString(R.string.fri),
                getString(R.string.sat) };

        // Gets Movie name, movie d
        Intent intent = getIntent();
        if (intent == null)
            throw new NullPointerException("no intent");

        // Gets movie name
        movieId = intent.getExtras().getInt(Constants.KEY_MOVIE_ID);
        movieName = intent.getExtras().getString(Constants.KEY_MOVIE_NAME);
        cinemaName = intent.getExtras().getString(Constants.KEY_CINEMA_NAME);
        showtimeId = intent.getExtras().getInt(Constants.KEY_SHOWTIME_ID);

        ((TextView) findViewById(R.id.movie_name)).setText(movieName);

        boolean fromAlarm = intent.getBooleanExtra(Constants.KEY_FROM_ALARM,
                false);
        if (fromAlarm) {
            // look up the notification manager service
            NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            // cancel the notification that we started in IncomingMessage
            nm.cancel(intent.getIntExtra(Constants.KEY_ALARM_NUMBER, 0));
            MtimeUtils.cancelEventInActivity(this, MovieSessionSelectedActivity.class,
                    getIntent().getIntExtra(Constants.KEY_ALARM_NUMBER, -1));
        }
    }

    private String getDetails(String cinemaName, String movieName) {
        String movieDetails = "";
        Calendar date = Calendar.getInstance(Locale.CHINA);
        date.setTimeInMillis(mShowtime.getTime());
        movieDetails += MtimeUtils.getSimpleDate(mShowtime.getTime());
        movieDetails += " " + weekChinese[date.get(Calendar.DAY_OF_WEEK)];
        movieDetails += " " + MtimeUtils.getTime(mShowtime.getTime());
        movieDetails += " " + movieName;
        movieDetails += " " + cinemaName;
        movieDetails += " " + mShowtime.getVersion();
        if (mShowtime.getDuration() > 0)
            movieDetails += "(" + mShowtime.getDuration()
                    + getString(R.string.minutes) + ")";

        if (mShowtime.getPrice() != null && mShowtime.getPrice().length() > 0)
            movieDetails += " " + getString(R.string.ticket_price) + ":"
                    + mShowtime.getPrice() + getString(R.string.yuan);

        return movieDetails;
    }

    @Override
    void doOperation() {
        mShowtime = rs.getShowtimeDetail(showtimeId);
        // Show movie details.
        movieDetails = getDetails(cinemaName, movieName);
        ((TextView) findViewById(R.id.text_movie_detail)).setText(movieDetails);
        // Initiate the "calendar" button
        Button insertCalendarBtn = (Button) findViewById(R.id.btn_insert_calenar);

        loginCookies = MtimeUtils.getCookies();
        isLogin = false;
        if (null != loginCookies && 0 != loginCookies.size())
            isLogin = true;
        isRatingOn = false;
        if (isLogin
                && MtimeUtils.getNotifySetting(
                        MovieSessionSelectedActivity.this,
                        Constants.KEY_SETTING_RATING_NOTIFY))
            isRatingOn = true;
        isComingOn = false;
        if (isLogin
                && MtimeUtils.getNotifySetting(
                        MovieSessionSelectedActivity.this,
                        Constants.KEY_SETTING_COMING_NOTIFY))
            isComingOn = true;

        insertCalendarBtn.setEnabled(false);
        // If user didn't login or if user login and have alarm
        if (!isLogin || isComingOn || isRatingOn)
            insertCalendarBtn.setEnabled(true);
        insertCalendarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // Use alarm helper to show alarm dialog.
                showDialog(R.id.btn_insert_calenar);
            }
        });
        // Initiate the "SMS somebody" button
        ((Button) findViewById(R.id.btn_insert_sms))
                .setOnClickListener(new View.OnClickListener() {
                    public void onClick(View arg0) {
                        // Start
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_SEND);
                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                        intent.setType("text/plain");
                        intent.putExtra("sms_body", movieDetails);
                        MovieSessionSelectedActivity.this.startActivity(intent);
                    }
                });
        // Initiate the button text
        Button cinema = (Button) findViewById(R.id.btn_cinima_shop);
        cinema.setText(cinemaName);
        // Hide
        cinema.setVisibility(View.GONE);
        ((Button) findViewById(R.id.btn_all_films))
                .setOnClickListener(new View.OnClickListener() {
                    public void onClick(View arg0) {
                        Intent i = new Intent(
                                MovieSessionSelectedActivity.this,
                                MovieRecentListActivity.class);
                        startActivity(i);
                    }
                });
    }

}

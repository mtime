package com.mtime;

import com.mtime.remote.DataServiceFactory;

import com.mtime.remote.RemoteService;

import android.app.Activity;
import android.os.Bundle;

public abstract class AbstractMtimeHandleActivity extends Activity {

    boolean mNeedLoad;
    RemoteService rs = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rs = DataServiceFactory.getRemoteService(this);
        mNeedLoad = true;
    }

    // Please don't implement onStart on the sub-class, just do whatever you
    // want in the doOperation();
    @Override
    protected void onStart() {
        super.onStart();
        if (mNeedLoad) {
            doOperation();
            // If do it successfully, don't need to load again.
            mNeedLoad = false;
        }
    }

    abstract void doOperation();
}

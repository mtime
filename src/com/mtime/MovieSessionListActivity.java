package com.mtime;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.mtime.AbstractMtimeActivity.Task;
import com.mtime.data.Movie;
import com.mtime.data.Showtime;
import com.mtime.util.MtimeUtils;

public class MovieSessionListActivity extends AbstractMtimeActivity {

	int movieId;
	Movie movie;
	String movieName, cinemaName;
	TextView navName;

	private ViewFlipper vflipper;
	private int cinemaId;
	private int date;
	Button bt_Today, bt_Tomorrow, bt_AfterTomorrow;

	final String[] days = MtimeUtils.getDays(3);

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.act_movie_session_list);

		navName = (TextView) findViewById(R.id.tv_movieName);
		vflipper = (ViewFlipper) findViewById(R.id.flipper);
		movieId = getIntent().getExtras().getInt(Constants.KEY_MOVIE_ID);

		cinemaId = getIntent().getExtras().getInt(Constants.KEY_CINEMA_ID);
		cinemaName = getIntent().getExtras().getString(
				Constants.KEY_CINEMA_NAME);
		movieName = getIntent().getExtras().getString(Constants.KEY_MOVIE_NAME);

		navName.setText(movieName);
		date = getIntent().getExtras().getInt(Constants.KEY_DATE);
		if (date == 0)
			date = Integer.parseInt(days[0]);

	}

	@Override
	protected void onStart() {
		super.onStart();
		// Override mainBody
		mainBody = findViewById(R.id.sessionsList);
	}

	private void setBtnSelected(Button btn) {
		btn.setBackgroundResource(R.drawable.btn_date_background_selected);
		btn.setClickable(false);
	}

	private void setBtnUnSelected(Button btn) {
		btn.setBackgroundResource(R.drawable.btn_date_background_default);
		btn.setClickable(true);
	}

	private void refreshButtonBg(int selectedDate) {

		if (selectedDate == Integer.parseInt(days[0])) {
			setBtnSelected(bt_Today);
			setBtnUnSelected(bt_Tomorrow);
			setBtnUnSelected(bt_AfterTomorrow);
		} else if (selectedDate == Integer.parseInt(days[1])) {
			setBtnUnSelected(bt_Today);
			setBtnSelected(bt_Tomorrow);
			setBtnUnSelected(bt_AfterTomorrow);
		} else if (selectedDate == Integer.parseInt(days[2])) {
			setBtnUnSelected(bt_Today);
			setBtnUnSelected(bt_Tomorrow);
			setBtnSelected(bt_AfterTomorrow);
		}

	}

	private void setUpMovieSessionListView(final int cinemaId, final int date) {

		((TextView) findViewById(R.id.tv_CinemaName)).setText(cinemaName);

		bt_Today = (Button) findViewById(R.id.bt_Today);
		bt_Tomorrow = (Button) findViewById(R.id.bt_Tomorrow);
		bt_AfterTomorrow = (Button) findViewById(R.id.bt_AfterTomorrow);

		bt_Today.setText(MtimeUtils.getDayDesc(days[0]));
		bt_Tomorrow.setText(MtimeUtils.getDayDesc(days[1]));
		bt_AfterTomorrow.setText(MtimeUtils.getDayDesc(days[2]));

		OnClickListener lsner = new OnClickListener() {
			@Override
			public void onClick(View view) {
				int date = 0;
				switch (view.getId()) {
				case R.id.bt_Today:
					date = Integer.parseInt(days[0]);
					break;
				case R.id.bt_Tomorrow:
					date = Integer.parseInt(days[1]);
					break;
				case R.id.bt_AfterTomorrow:
					date = Integer.parseInt(days[2]);
					break;

				}
				setUpMovieSessionListView(cinemaId, date);
			}
		};

		bt_Today.setOnClickListener(lsner);
		bt_Tomorrow.setOnClickListener(lsner);
		bt_AfterTomorrow.setOnClickListener(lsner);

		Button cinemaDetailBtn = (Button) findViewById(R.id.cinemaDetailBtn);
		cinemaDetailBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent();
				i.setClass(MovieSessionListActivity.this,
						CinemaDetailActivity.class);
				i.putExtra(Constants.KEY_CINEMA_ID, cinemaId);
				i.putExtra(Constants.KEY_CINEMA_NAME, cinemaName);
				MovieSessionListActivity.this.startActivity(i);
			}
		});

		Button cinemaListBtn = (Button) findViewById(R.id.cinemaListBtn);
		cinemaListBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent();
				i.setClass(MovieSessionListActivity.this,
						MovieCinemaListActivity.class);

				i.putExtra(Constants.KEY_MOVIE_ID, movieId);
				i.putExtra(Constants.KEY_MOVIE_NAME, movieName);

				i.putExtra(Constants.KEY_CINEMA_ID, 0);
				i.putExtra(Constants.KEY_CINEMA_NAME, "");

				// i.putExtra(Constants.KEY_CINEMA_NAME, cinemaName);

				MovieSessionListActivity.this.startActivity(i);

			}
		});

		refreshButtonBg(date);
		new Task() {
			List<Showtime> list;

			@Override
			public void after() {
				setUpMovieSessionListView(cinemaId, date, list);
			}

			@SuppressWarnings("unchecked")
			@Override
			public void before() throws Exception {
				list = rs.getCinemaMovieShowtimes(currentLocation.getId(),
						movieId, date, cinemaId);
				Collections.sort(list);
			}
		}.start();
	}

	private void setUpMovieSessionListView(final int cinemaId, int date,
			final List<Showtime> list) {
		Log.d(Constants.LOGTAG, "setUpMovieSessionListView");

		LinearLayout radios = (LinearLayout) findViewById(R.id.sessionsList);
		radios.removeAllViews();

		if (list.size() == 0) {
			emptyAlert.setVisibility(View.VISIBLE);
			return;
		} else {
			emptyAlert.setVisibility(View.GONE);
		}

		LinearLayout row = null;
		final List<RadioButton> btnList = new ArrayList<RadioButton>();

		OnClickListener sessionItemClickListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				int pos = 0;
				for (int i = 0; i < btnList.size(); i++) {
					RadioButton btn = btnList.get(i);
					if (btn.getId() != v.getId())
						btn.setChecked(false);
					else
						pos = i;

				}

				Intent i = new Intent();
				i.setClass(MovieSessionListActivity.this,
						MovieSessionDetailActivity.class);

				i.putExtra(Constants.KEY_MOVIE_ID, movieId);
				i.putExtra(Constants.KEY_MOVIE_NAME, movieName);
				i.putExtra(Constants.KEY_CINEMA_NAME, cinemaName);
				i.putExtra(Constants.KEY_SHOWTIME_LIST, (Serializable) list);
				i.putExtra(Constants.KEY_POS_IN_SHOWTIME_LIST, pos);

				MovieSessionListActivity.this.startActivity(i);
			}

		};

		for (int i = 0; i < list.size(); i++) {
			if (i % 2 == 0) {
				row = new LinearLayout(this);
				row.setOrientation(LinearLayout.HORIZONTAL);
			}

			final Showtime showtime = list.get(i);
			RadioButton rb = new RadioButton(this);
			rb.setId(showtime.getId());
			rb.setText(MtimeUtils.getTime(showtime.getTime()));
			rb.setWidth(140);
			rb.setTextColor(Color.BLUE);
			rb.setTextSize(16f);
			if (showtime.getTime() < System.currentTimeMillis()) {
				rb.setEnabled(false);
				rb.setTextColor(Color.GRAY);
			}

			rb.setOnClickListener(sessionItemClickListener);

			btnList.add(rb);
			row.addView(rb);

			if (i % 2 == 1) {
				radios.addView(row);

				if (i < list.size() - 1) {
					ImageView img = new ImageView(MovieSessionListActivity.this);
					img.setBackgroundResource(R.drawable.tmp_divider);
					radios.addView(img);
					// radios.addView(getLayoutInflater().inflate(
					// R.layout.view_divider, null));
				}
			}

		}
		if (list.size() % 2 == 1)
			radios.addView(row);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog dialog = super.onCreateDialog(id);
		if (dialog != null)
			return dialog;

		switch (id) {

		case DIALOG_NO_MOVIE_IN_THIS_CINEMA: {
			return new AlertDialog.Builder(MovieSessionListActivity.this)
					.setTitle(R.string.no_movie_in_this_cinema).setIcon(
							android.R.drawable.ic_dialog_info)
					.setNeutralButton(R.string.check_other_cinema,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int whichButton) {
									Intent i = new Intent();
									i.setClass(MovieSessionListActivity.this,
											MovieSessionListActivity.class);

									i.putExtra(Constants.KEY_MOVIE_ID, movieId);
									// i.putExtra(Constants.KEY_CINEMA_NAME,
									// cinemaName);

									startActivity(i);
									finish();
								}
							}).create();

		}
		}
		return null;
	}

	private void showMovieDetailView(final int movieId) {
		new Thread() {
			@Override
			public void run() {
				movie = rs.getMovieDetail(movieId);
				Message message = movieDetailHandler.obtainMessage(0, this);
				movieDetailHandler.sendMessage(message);			}
		}.start();
	}
	
	final Handler movieDetailHandler = new Handler() {
		@Override
		public void handleMessage(Message message) {
			if (null == movie)
				MtimeUtils.gotoErrorActivity(MovieSessionListActivity.this);
			else
				MovieDetailActivity.showMovieDetailView(
						MovieSessionListActivity.this, movie, vflipper);
		}		
	};

	@Override
	void doOperation() {
		setUpMovieSessionListView(cinemaId, date);
		showMovieDetailView(movieId);
	}
}
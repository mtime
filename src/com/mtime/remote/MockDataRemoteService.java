package com.mtime.remote;

import java.io.ByteArrayOutputStream;

import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.content.res.AssetManager;

public class MockDataRemoteService extends RemoteService {

    private AssetManager manager;

    public MockDataRemoteService(Activity activity) {
        this.manager = activity.getAssets();
    }

    @Override
    String getLocationCinemasString(int locationId) {
        return get(manager, buildFileName("LocationCinemas"));
    }

    @Override
    String getLocationMoviesString(int locationId) {
        return get(manager, buildFileName("LocationMovies"));
    }

    @Override
    String getMovieDetailString(int movieId) {
        return get(manager, buildFileName("MovieDetail"));
    }

    @Override
    String geCinemaMoviesString(int locationId, int cinemaId) {
        return get(manager, buildFileName("CinemaMovies"));
    }

    @Override
    String getCinemaDetailString(int cinemaId) {
        return get(manager, buildFileName("CinemaDetail"));
    }

    @Override
    String getChinaLocationsString() {
        return get(manager, buildFileName("ChinaLocations"));
    }

    @Override
    String getShowtimeDetailString(int showtimeId) {
        return get(manager, buildFileName("ShowtimeDetail"));
    }

    @Override
    String getCinemaMovieShowtimesString(int locationId, int movieId, int date,
            int cinemaId) {
        return get(manager, buildFileName("CinemaMovieShowtimes"));
    }

    @Override
    String getLocationMovieShowtimesString(int locationId, int movieId, int date) {
        return get(manager, buildFileName("LocationMovieShowtimes"));
    }

    private static String buildFileName(String f) {
        return "mockdata/" + f + ".txt";
    }

    public static String get(AssetManager manager, String fileName) {
        String result = "";
        try {
            result = readTextFile(manager.open(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private static String readTextFile(InputStream inputStream) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte buf[] = new byte[1024];
        int len;
        try {
            while ((len = inputStream.read(buf)) != -1) {
                outputStream.write(buf, 0, len);
            }
            outputStream.close();
            inputStream.close();
        } catch (IOException e) {
        }
        return outputStream.toString();
    }

    @Override
    String getComingMoviesString() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    String getNearByCinemasString(double geoLatitude, double geoLongitude) {
        return get(manager, buildFileName("NearbyLocations"));
    }

    @Override
    String getSignInString(String email, String password) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    String getRatingMovieString(int movieId, int ratingScore,
            int inpressRatingScore, int storyRatingScore, int showRatingScore,
            int directorRatingScore, int screenRatingScore,
            int musicRatingScore, String comment) {
        // TODO Auto-generated method stub
        return null;
    }
}

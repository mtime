package com.mtime.remote;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import android.util.Log;

import com.mtime.Constants;
import com.mtime.data.SerializableCookie;
import com.mtime.util.FileLocalCache;
import com.mtime.util.MtimeUtils;

public class ConnectUtil {
	private static final String TAG = "CURL";

	public static String post(String url, Map<String, String> params)
			throws Exception {
		HttpPost httpost = new HttpPost(url);

		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		String postData = "";
		for (String key : params.keySet()) {
			nvps.add(new BasicNameValuePair(key, params.get(key)));
			try {
				postData += key + "="
						+ URLEncoder.encode(params.get(key), "UTF-8") + "&";
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (postData.length() > 1)
			postData = postData.substring(0, postData.length() - 1);

		try {
			httpost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
		} catch (UnsupportedEncodingException e) {
		}
		return execute(httpost, postData);
	}

	public static String get(String url) throws Exception {

		Log.d(TAG, url);
		String s = FileLocalCache.load2(url);
		if (s != null && s.length() > 0) {
			return s;
		}

		HttpGet httpget = new HttpGet(url);
		s = execute(httpget, "");

		if (s == null || s.length() == 0)
			throw new Exception("can't get content from url");
		FileLocalCache.store(url, s);

		return s;

	}

	private static String convertStreamToString(InputStream is)
			throws Exception {
		/*
		 * To convert the InputStream to String we use the
		 * BufferedReader.readLine() method. We iterate until the BufferedReader
		 * return null which means there's no more data to read. Each line will
		 * appended to a StringBuilder and returned as String.
		 */
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}

		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	private static String execute(HttpUriRequest req, String postData)
			throws Exception {
		long startTime = System.currentTimeMillis();

		HttpClient httpclient = new DefaultHttpClient();
		httpclient.getParams().setParameter("http.protocol.content-charset",
				"UTF-8");

		// Execute the request
		String result = null;
		HttpResponse response = null;
		InputStream instream = null;
		try {
			// if (req instanceof HttpPost) {
			List<Cookie> storedCookies = MtimeUtils.getCookies();
			Log.i(TAG, "storedCookies="+storedCookies);
			if (storedCookies != null) {
				BasicCookieStore cs = new BasicCookieStore();
				cs
						.addCookies((Cookie[]) storedCookies
								.toArray(new Cookie[] {}));
				((AbstractHttpClient) httpclient).setCookieStore(cs);
			}
			// CheckValue = MD5(AppId+ClientKey+TimeStamp+Query+PostData)
			String checkValue = FileLocalCache.md5(Constants.APPID
					+ Constants.CLIENT_KEY + startTime
					+ req.getURI().toString() + postData);

			// Header: X-MTime-Showtime-CheckValue:
			// AppId,Timestamp,CheckValue
			String header = Constants.APPID + "," + startTime + ","
					+ checkValue;
			req.addHeader("X-MTime-Showtime-CheckValue", header);
			req.addHeader("Accept-Charset", "UTF-8,*");
			// }

			response = httpclient.execute(req);
			HttpEntity entity = response.getEntity();

			if (entity != null) {
				instream = entity.getContent();
				result = convertStreamToString(instream);
			}

			// handle cookie stuff
			List<Cookie> cookies = ((AbstractHttpClient) httpclient)
					.getCookieStore().getCookies();
			if (!cookies.isEmpty()) {
				// store cookies
				final List<Cookie> serialisableCookies = new ArrayList<Cookie>(
						cookies.size());
				for (Cookie cookie : cookies) {
					serialisableCookies.add(new SerializableCookie(cookie));
				}
				MtimeUtils.setCookies(serialisableCookies);
			}

		} finally {
			try {
				instream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		long endTime = System.currentTimeMillis();
		Log.d(TAG, "Time:" + (endTime - startTime) + "@" + req.getURI());
		return result;
	}

}

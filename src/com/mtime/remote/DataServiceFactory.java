package com.mtime.remote;

import android.app.Activity;
import com.mtime.Constants;

public class DataServiceFactory {
    public static RemoteService getRemoteService(Activity activity) {
        switch (Constants.NOW_MODE) {
        case MOCK_DATA:
            return new MockDataRemoteService(activity);
        default:
            return new HttpDataRemoteService(activity);
        }
    }
}
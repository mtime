package com.mtime.remote;

import java.util.HashMap;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;

import android.app.Activity;
import com.mtime.data.Cinema;
import com.mtime.data.Locations;
import com.mtime.data.Movie;
import com.mtime.data.RatingResult;
import com.mtime.data.Showtime;
import com.mtime.data.SignInResult;
import com.mtime.util.MtimeUtils;

public abstract class RemoteService {

	protected Activity mActivity;

	/**
	 * 获得某城市的所有影院信息
	 * 
	 * @param locationId
	 * @return
	 */
	abstract String getLocationCinemasString(int locationId);

	/**
	 * 获得某城市正在上映的影片信息
	 * 
	 * @param locationId
	 * @return
	 */
	abstract String getLocationMoviesString(int locationId);

	/**
	 * 获得某影院正在上映的影片信息
	 * 
	 * @param locationId
	 * @param cinemaId
	 * @return
	 */
	abstract String geCinemaMoviesString(int locationId, int cinemaId);

	/**
	 * 获得某影片详细信息
	 * 
	 * @param movieId
	 * @return
	 */
	abstract String getMovieDetailString(int movieId);

	/**
	 * 获得某影院详细信息
	 * 
	 * @param cinemaId
	 * @return
	 */
	abstract String getCinemaDetailString(int cinemaId);

	/**
	 * 获得中国地区信息（2级，直辖市不分区）
	 * 
	 * @return
	 */
	abstract String getChinaLocationsString();

	/**
	 * 返回某场次影讯的详细消息
	 * 
	 * @param showtimeId
	 * @return
	 */
	abstract String getShowtimeDetailString(int showtimeId);

	/**
	 * 返回某影院某影片某日所有场次的影讯列表
	 * 
	 * @param locationId
	 * @param movieId
	 * @param date
	 * @param cinemaId
	 * @return
	 */
	abstract String getCinemaMovieShowtimesString(int locationId, int movieId,
			int date, int cinemaId);

	/**
	 * 返回某地区某影片某日，有影讯的影院列表
	 * 
	 * @param locationId
	 * @param movieId
	 * @param date
	 * @return
	 */
	abstract String getLocationMovieShowtimesString(int locationId,
			int movieId, int date);

	/**
	 * 获取最近上映的影片列表
	 * 
	 * @return
	 */
	abstract String getComingMoviesString();

	/**
	 * 获得附近的影院列表
	 * 
	 * @param geoLatitude
	 * @param geoLongitude
	 * @return
	 */
	abstract String getNearByCinemasString(double geoLatitude,
			double geoLongitude);

	/**
	 * 获得登录结果
	 * 
	 * @param email
	 * @param password
	 * @return
	 */
	abstract String getSignInString(String email, String password);

	/**
	 * 评分结果
	 * 
	 * @param c
	 * @return
	 */
	abstract String getRatingMovieString(int movieId, int ratingScore,
			int inpressRatingScore, int storyRatingScore, int showRatingScore,
			int directorRatingScore, int screenRatingScore,
			int musicRatingScore, String comment);

	public List<Showtime> getCinemaMovieShowtimes(int locationId, int movieId,
			int date, int cinemaId) {
		String c = getCinemaMovieShowtimesString(locationId, movieId, date,
				cinemaId);
		List<Showtime> result = null;
		try {
			result = DataTransformUtils.getCinemaMovieShowtimes(c);
		} catch (JSONException e) {
			e.printStackTrace();
			MtimeUtils.gotoErrorActivity(mActivity);
		}
		return result;
	}

	public Showtime getShowtimeDetail(int showtimeId) {
		String c = getShowtimeDetailString(showtimeId);
		Showtime results = null;
		try {
			results = DataTransformUtils.getShowtimeDetail(c);
		} catch (JSONException e) {
			e.printStackTrace();
			MtimeUtils.gotoErrorActivity(mActivity);
		}
		return results;
	}

	public Locations getLocations(int locationId) {
		// TODO...
		return null;
	}

	public List<Locations> getChinaLocations() {
		String c = getChinaLocationsString();
		List<Locations> results = null;
		try {
			results = DataTransformUtils.getChinaLocationsString(c);
		} catch (JSONException e) {
			e.printStackTrace();
			MtimeUtils.gotoErrorActivity(mActivity);
		}
		return results;
	}

	public List<Cinema> getLocationCinemas(int locationId) {
		String c = getLocationCinemasString(locationId);
		List<Cinema> results = null;
		try {
			results = DataTransformUtils.getLocationCinemas(locationId, c);
		} catch (JSONException e) {
			e.printStackTrace();
			MtimeUtils.gotoErrorActivity(mActivity);
		}
		return results;
	}

	public List<Cinema> getNearByCinemas(double geoLatitude, double geoLongitude) {
		String c = getNearByCinemasString(geoLatitude, geoLongitude);
		List<Cinema> results = null;
		try {
			results = DataTransformUtils.getNearByCinemas(c);
		} catch (JSONException e) {
			e.printStackTrace();
			MtimeUtils.gotoErrorActivity(mActivity);
		}
		return results;
	}

	public LinkedHashMap<String, List<Cinema>> getLocationMovieShowtimes(
			int locationId, int movieId, int date) {
		String c = getLocationMovieShowtimesString(locationId, movieId, date);
		LinkedHashMap<String, List<Cinema>> results = null;
		try {
			results = DataTransformUtils.getLocationMovieShowtimes(c);
		} catch (JSONException e) {
			e.printStackTrace();
			MtimeUtils.gotoErrorActivity(mActivity);
		}
		return results;
	}

	public String getBackgroundAdsImage(int locationId) {
		String c = getLocationMoviesString(locationId);
		String results = "";
		try {
			results = DataTransformUtils.getBackgroundAdsImage(c);
		} catch (JSONException e) {
			e.printStackTrace();
			MtimeUtils.gotoErrorActivity(mActivity);
		}
		return results;
	}

	public List<Movie> getLocationMovies(int locationId) {

		String c = getLocationMoviesString(locationId);
		List<Movie> results = null;
		try {
			results = DataTransformUtils.getLocationMovies(c);
		} catch (JSONException e) {
			e.printStackTrace();
			MtimeUtils.gotoErrorActivity(mActivity);
		}
		return results;
	}

	public Map<String, Object> getAdsImageAndLocationMovies(int locationId) {
		Map<String, Object> result = new HashMap<String, Object>();

		String c = getLocationMoviesString(locationId);
		try {
			String adsImage = DataTransformUtils.getBackgroundAdsImage(c);
			List<Movie> movies = DataTransformUtils.getLocationMovies(c);
			result.put("adsImage", adsImage);
			result.put("movies", movies);
		} catch (JSONException e) {
			e.printStackTrace();
			MtimeUtils.gotoErrorActivity(mActivity);
		}
		return result;
	}

	public Cinema getCinemaDetail(int cinemaId) {

		String c = getCinemaDetailString(cinemaId);
		Cinema results = null;
		try {
			results = DataTransformUtils.getCinemaDetail(c);
		} catch (JSONException e) {
			e.printStackTrace();
			MtimeUtils.gotoErrorActivity(mActivity);
		}
		return results;

	}

	public List<Movie> getCinemaMovies(int locationId, int cinemaId) {

		String c = geCinemaMoviesString(locationId, cinemaId);
		List<Movie> results = null;
		try {
			results = DataTransformUtils.getCinemaMovies(c);
		} catch (JSONException e) {
			e.printStackTrace();
			MtimeUtils.gotoErrorActivity(mActivity);
		}
		return results;

	}

	public List<Movie> getComingMovies() {

		String c = getComingMoviesString();
		List<Movie> results = null;
		try {
			results = DataTransformUtils.getComingMovies(c);
		} catch (JSONException e) {
			e.printStackTrace();
			MtimeUtils.gotoErrorActivity(mActivity);
		}
		return results;

	}

	public Movie getMovieDetail(int movieId) {

		String c = getMovieDetailString(movieId);
		Movie results = null;
		try {
			results = DataTransformUtils.getMovieDetail(c);
		} catch (JSONException e) {
			e.printStackTrace();
			MtimeUtils.gotoErrorActivity(mActivity);
		}
		return results;

	}

	public SignInResult getSignInResult(String email, String password) {

		String c = getSignInString(email, password);
		SignInResult results = null;
		try {
			results = DataTransformUtils.getSignInResult(c);
		} catch (JSONException e) {
			e.printStackTrace();
			MtimeUtils.gotoErrorActivity(mActivity);
		}
		return results;

	}

	//
	// post body: movieid=69283&r=9&ir=8&str=9&shr=10&dr=10&pr=8&mr=9&c=good
	// r: 总评分
	// ir: 印象分
	// str: 故事分
	// shr: 表演分
	// dr: 导演分
	// pr: 画面分
	// mr: 音乐分
	// c: 一句话影评

	public RatingResult getRatingMovie(int movieId, int ratingScore,
			int inpressRatingScore, int storyRatingScore, int showRatingScore,
			int directorRatingScore, int screenRatingScore,
			int musicRatingScore, String comment) {

		String c = getRatingMovieString(movieId, ratingScore,
				inpressRatingScore, storyRatingScore, showRatingScore,
				directorRatingScore, screenRatingScore, musicRatingScore,
				comment);
		RatingResult results = null;
		try {
			results = DataTransformUtils.getRatingMovie(c);
		} catch (JSONException e) {
			e.printStackTrace();
			MtimeUtils.gotoErrorActivity(mActivity);
		}
		return results;
	}
}

package com.mtime.remote;

import java.util.Formatter;

import java.util.LinkedHashMap;
import java.util.Map;

import com.mtime.util.MtimeUtils;

import android.app.Activity;

public class HttpDataRemoteService extends RemoteService {
	private static String prefix = "http://api.mtime.com/";

	public HttpDataRemoteService(Activity activity) {
		mActivity = activity;
	}

	@SuppressWarnings("unused")
	private String getFormatUrl(String fmt, int... i) {
		StringBuilder sb = new StringBuilder();
		Formatter formatter = new Formatter(sb);
		formatter.format(fmt, i);
		return prefix + sb.toString();

	}

	@Override
	String getLocationCinemasString(int locationId) {
		// http://api.mtime.com/showtime/{locationId}/cinemas/
		String url = prefix + "showtime/" + locationId + "/cinemas/";
		return getHttpGetResponse(url);
	}

	@Override
	String getLocationMoviesString(int locationId) {
		// http://api.mtime.com/showtime/{locationId}/movies/
		String url = prefix + "showtime/" + locationId + "/movies/";
		return getHttpGetResponse(url);
	}

	@Override
	String getMovieDetailString(int movieId) {
		// http://api.mtime.com/showtime/movie/{movieId}/
		String url = prefix + "showtime/movie/" + movieId + "/";
		return getHttpGetResponse(url);
	}

	@Override
	String geCinemaMoviesString(int locationId, int cinemaId) {
		// http://api.mtime.com/showtime/{locationId}/{cinemaId}/movies/
		String url = prefix + "showtime/" + locationId + "/" + cinemaId
				+ "/movies/";
		return getHttpGetResponse(url);
	}

	@Override
	String getCinemaDetailString(int cinemaId) {
		// http://api.mtime.com/showtime/cinema/{cinemaId}/
		String url = prefix + "showtime/cinema/" + cinemaId + "/";
		return getHttpGetResponse(url);
	}

	@Override
	String getChinaLocationsString() {
		String url = prefix + "showtime/chinalocations/";
		return getHttpGetResponse(url);
	}

	@Override
	String getShowtimeDetailString(int showtimeId) {
		String url = prefix + "showtime/detail/" + showtimeId + "/";
		return getHttpGetResponse(url);
	}

	@Override
	String getCinemaMovieShowtimesString(int locationId, int movieId, int date,
			int cinemaId) {
		// http://api.mtime.com/showtime/{locationId}/{movieId}/{date}/{cinemaId}/
		String url = prefix + "showtime/" + locationId + "/" + movieId + "/"
				+ date + "/" + cinemaId + "/";
		return getHttpGetResponse(url);
	}

	@Override
	String getLocationMovieShowtimesString(int locationId, int movieId, int date) {
		// http://api.mtime.com/showtime/{locationId}/{movieId}/{date}/
		String url = prefix + "showtime/" + locationId + "/" + movieId + "/"
				+ date + "/";
		return getHttpGetResponse(url);
	}

	@Override
	String getComingMoviesString() {
		// http://api.mtime.com/showtime/comingmovies/
		String url = prefix + "showtime/comingmovies/";
		return getHttpGetResponse(url);
	}

	@Override
	String getNearByCinemasString(double geoLatitude, double geoLongitude) {
		// query: http://api.mtime.com/showtime/searchcinemasnearby.api
		// post body: addrn=35.39746&addre=116.5702
		String url = prefix + "showtime/searchcinemasnearby.api";

		Map<String, String> paras = new LinkedHashMap<String, String>();
		paras.put("addrn", String.valueOf(geoLatitude));
		paras.put("addre", String.valueOf(geoLongitude));

		return getHttpPostResponse(url, paras);
	}

	@Override
	String getSignInString(String email, String password) {
		String url = prefix + "showtime/signin.api";

		Map<String, String> paras = new LinkedHashMap<String, String>();
		paras.put("email", email);
		paras.put("password", password);

		return getHttpPostResponse(url, paras);
	}

	@Override
	String getRatingMovieString(int movieId, int ratingScore,
			int inpressRatingScore, int storyRatingScore, int showRatingScore,
			int directorRatingScore, int screenRatingScore,
			int musicRatingScore, String comment) {
		// r: 总评分
		// ir: 印象分
		// str: 故事分
		// shr: 表演分
		// dr: 导演分
		// pr: 画面分
		// mr: 音乐分
		// c: 一句话影评
		String url = prefix + "showtime/ratingmovie.api";

		Map<String, String> paras = new LinkedHashMap<String, String>();
		paras.put("movieid", String.valueOf(movieId));
		paras.put("r", String.valueOf(ratingScore));
		paras.put("ir", String.valueOf(inpressRatingScore));
		paras.put("str", String.valueOf(storyRatingScore));
		paras.put("shr", String.valueOf(showRatingScore));
		paras.put("dr", String.valueOf(directorRatingScore));
		paras.put("pr", String.valueOf(screenRatingScore));
		paras.put("mr", String.valueOf(musicRatingScore));
		paras.put("c", comment);
		return getHttpPostResponse(url, paras);
	}

	private String getHttpGetResponse(String url) {
		String result = "";
		try {
			result = ConnectUtil.get(url);
		} catch (Exception e) {
			// If there's network error occur, just go to network error
			// activity
			e.printStackTrace();
			MtimeUtils.gotoErrorActivity(mActivity);
		}
		return result;
	}

	private String getHttpPostResponse(String url, Map<String, String> paras) {
		String result = "";
		try {
			result = ConnectUtil.post(url, paras);
		} catch (Exception e) {
			// If there's network error occur, just go to network error
			// activity
			e.printStackTrace();
			MtimeUtils.gotoErrorActivity(mActivity);
		}
		return result;
	}
}

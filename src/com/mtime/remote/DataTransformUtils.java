package com.mtime.remote;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.mtime.data.Cinema;
import com.mtime.data.Comment;
import com.mtime.data.Locations;
import com.mtime.data.Movie;
import com.mtime.data.RatingResult;
import com.mtime.data.Showtime;
import com.mtime.data.SignInResult;

public class DataTransformUtils {
	private static boolean getBoolean(JSONObject o, String name)
			throws JSONException {
		if (!o.isNull(name))
			return o.getBoolean(name);
		return false;
	}

	private static int getInt(JSONObject o, String name) throws JSONException {
		if (!o.isNull(name))
			return o.getInt(name);
		return 0;
	}

	private static long getLong(JSONObject o, String name) throws JSONException {
		if (!o.isNull(name))
			return o.getLong(name);
		return 0l;
	}

	private static double getDouble(JSONObject o, String name)
			throws JSONException {
		if (!o.isNull(name))
			return o.getDouble(name);
		return 0d;
	}

	private static String getString(JSONObject o, String name)
			throws JSONException {
		if (!o.isNull(name))
			return o.getString(name);
		return "";
	}

	public static List<Showtime> getCinemaMovieShowtimes(String cx)
			throws JSONException {
		List<Showtime> list = new ArrayList<Showtime>();

		JSONObject j = new JSONObject(cx);
		if (!j.isNull("s")) {
			JSONArray array = j.getJSONArray("s");
			for (int i = 0; i < array.length(); i++) {
				Showtime s = new Showtime();
				JSONObject o = array.getJSONObject(i);
				s.setId(getInt(o, "sid"));
				s.setTime(getLong(o, "s"));
				list.add(s);
			}
		}
		return list;
	}

	public static Showtime getShowtimeDetail(String cx) throws JSONException {
		Showtime s = new Showtime();
		JSONObject j = new JSONObject(cx);
		s.setTime(getLong(j, "s"));
		s.setPrice(getString(j, "p"));
		s.setLanguage(getString(j, "l"));
		s.setVersion(getString(j, "v"));
		s.setDuration(getInt(j, "d"));
		s.setHall(getString(j, "h"));
		return s;
	}

	public static List<Movie> getLocationMovies(String c) throws JSONException {
		//Log.d("DEF", c);
		List<Movie> list = new ArrayList<Movie>();

		List<Movie> recommandList = new ArrayList<Movie>();

		JSONObject j = new JSONObject(c);
		if (!j.isNull("ms")) {
			JSONArray array = j.getJSONArray("ms");
			for (int i = 0; i < array.length(); i++) {
				JSONObject o = array.getJSONObject(i);
				Movie m = new Movie();
				m.setId(getInt(o, "id"));
				m.setName(getString(o, "t"));
				m.setRatingScore(getDouble(o, "r"));
				m.setRatingCount(getInt(o, "rc"));
				m.setImageSrc(getString(o, "img"));
				m.setTrailerSrc(getString(o, "m"));
				m.setDirectorName(getString(o, "dN"));
				m.setActorName1(getString(o, "aN1"));
				m.setActorName2(getString(o, "aN2"));
				m.setDuration(getString(o, "d"));
				m.setCinemaCount(getInt(o, "cC"));
				m.setShowtimeCount(getInt(o, "sC"));
				m.setUserAttitude(getInt(o, "ua"));
				m.setDef(getInt(o, "def"));

				// Set the recommended movie tag
				if (m.getDef() == 1) {
					Log.d("DEF", "" + m.getId());
					recommandList.add(m);
				} else {
					list.add(m);
				}
			}
		}
		if (list.size() > 1) {
			list.addAll(1, recommandList);
		} else {
			list.addAll(recommandList);
		}

		return list;
	}

	public static String getBackgroundAdsImage(String c) throws JSONException {
		JSONObject j = new JSONObject(c);
		// ads = (String) j.get("bImg");
		return getString(j, "bImg");

	}

	public static List<Movie> getCinemaMovies(String c) throws JSONException {
		return getLocationMovies(c);
	}

	public static Movie getMovieDetail(String cs) throws JSONException {
		JSONObject j = new JSONObject(cs);

		Movie m = new Movie();
		m.setName(getString(j, "t"));
		m.setRatingScore(getDouble(j, "r"));
		m.setRatingCount(getInt(j, "rc"));
		m.setImageSrc(getString(j, "img"));
		m.setTrailerSrc(getString(j, "m"));
		m.setDirectorName(getString(j, "dN"));
		m.setActorName1(getString(j, "aN1"));
		m.setActorName2(getString(j, "aN2"));
		m.setDuration(getString(j, "d"));
		m.setSummary(getString(j, "pt"));

		List<Comment> comments = new ArrayList<Comment>();
		if (!j.isNull("cts")) {
			JSONArray array = j.getJSONArray("cts");
			for (int i = 0; i < array.length(); i++) {
				JSONObject o = array.getJSONObject(i);
				Comment c = new Comment();
				c.setContent(getString(o, "ce"));
				c.setAuthorNickname(getString(o, "ca"));
				c.setTimestamp(getLong(o, "cd"));
				c.setAuthorImgSrc(getString(o, "caimg"));
				c.setLocation(getString(o, "cal"));
				c.setRatingScore(getDouble(o, "cr"));
				comments.add(c);
			}
		}
		m.setComments(comments);
		return m;
	}

	public static Cinema getCinemaDetail(String cx) throws JSONException {
		Cinema c = new Cinema();
		JSONObject j = new JSONObject(cx);

		c.setName(getString(j, "n"));
		c.setAddress(getString(j, "addr"));
		c.setGeoLatitude(getDouble(j, "addrN"));
		c.setGeoLongitude(getDouble(j, "addrE"));
		c.setTel(getString(j, "tel"));
		c.setPostCode(getString(j, "pc"));
		c.setRoute(getString(j, "tR"));
		c.setRatingScore(getDouble(j, "r"));
		c.setRatingScore4Arround(getDouble(j, "rA"));
		c.setRatingScore4Equipment(getDouble(j, "rP"));
		c.setRatingScore4Comfort(getDouble(j, "rF"));
		c.setRatingScore4Traffic(getDouble(j, "rT"));
		c.setRatingScore4Sound(getDouble(j, "rS"));
		c.setRatingScore4Service(getDouble(j, "rSd"));
		c.setHasSpecialHandicappedAccess(getInt(j, "sA") == 1);
		c.setHasSpecialPark(getInt(j, "sP") == 1);
		c.setHasSpecialChildPlay(getInt(j, "sK") == 1);
		c.setHasSpecialFoodCourt(getInt(j, "sF") == 1);
		c.setHasSpecialLadderChair(getInt(j, "sH") == 1);
		c.setHasSpecialGameRoom(getInt(j, "sG") == 1);
		c.setHasSpecialSDDS(getInt(j, "sSDDS") == 1);
		c.setHasSpecialDTS(getInt(j, "sDTS") == 1);
		c.setHasSpecialDLP(getInt(j, "sDLP") == 1);
		c.setAnnounce(getString(j, "bns"));

		return c;
	}

	public static LinkedHashMap<String, List<Cinema>> getLocationMovieShowtimes(
			String cx) throws JSONException {
		LinkedHashMap<String, List<Cinema>> map = new LinkedHashMap<String, List<Cinema>>();

		JSONObject j = new JSONObject(cx);
		if (!j.isNull("cs")) {
			JSONArray array = j.getJSONArray("cs");
			for (int i = 0; i < array.length(); i++) {
				JSONObject o = array.getJSONObject(i);
				Cinema c = new Cinema();
				c.setId(getInt(o, "cid"));
				c.setName(getString(o, "cn"));
				c.setLocationName(getString(o, "ln"));
				c.setRemainingShowtimeCount(getInt(o, "sC"));

				List<Cinema> l = map.get(c.getLocationName());
				if (l == null) {
					l = new ArrayList<Cinema>();
					map.put(c.getLocationName(), l);
				}
				l.add(c);
			}
		}

		return map;
	}

	public static List<Cinema> getLocationCinemas(int locationId, String cx)
			throws JSONException {
		List<Cinema> list = new ArrayList<Cinema>();

		JSONObject j = new JSONObject(cx);
		if (!j.isNull("d")) {
			JSONArray array = j.getJSONArray("d");
			for (int i = 0; i < array.length(); i++) {
				JSONObject o = array.getJSONObject(i);
				if (!o.isNull("c")) {
					JSONArray array2 = o.getJSONArray("c");
					for (int k = 0; k < array2.length(); k++) {
						JSONObject o2 = array2.getJSONObject(k);
						Cinema c = new Cinema();
						c.setName(getString(o2, "n"));
						c.setId(getInt(o2, "id"));
						c.setLocationId(locationId);
						list.add(c);
					}
				}
			}
		}

		return list;
	}

	public static List<Locations> getChinaLocationsString(String c)
			throws JSONException {
		List<Locations> list = new ArrayList<Locations>();

		JSONObject j = new JSONObject(c);
		if (!j.isNull("p")) {
			JSONArray array = j.getJSONArray("p");
			for (int i = 0; i < array.length(); i++) {
				JSONObject o = array.getJSONObject(i);
				Locations l = new Locations();
				l.setId(getInt(o, "id"));
				l.setName(getString(o, "n"));
				if (!o.isNull("c")) {
					List<Locations> list2 = new ArrayList<Locations>();
					JSONArray array2 = o.getJSONArray("c");
					for (int k = 0; k < array2.length(); k++) {
						JSONObject o2 = array2.getJSONObject(k);
						Locations l2 = new Locations();
						l2.setId(getInt(o2, "id"));
						l2.setName(getString(o2, "n"));
						list2.add(l2);
					}
					l.setCities(list2);

				}

				list.add(l);
			}
		}

		return list;
	}

	public static List<Movie> getComingMovies(String c) throws JSONException {
		List<Movie> list = new ArrayList<Movie>();

		JSONObject j = new JSONObject(c);
		if (!j.isNull("ms")) {
			JSONArray array = j.getJSONArray("ms");
			for (int i = 0; i < array.length(); i++) {
				JSONObject o = array.getJSONObject(i);
				Movie m = new Movie();
				m.setId(getInt(o, "id"));
				m.setName(getString(o, "t"));
				m.setImageSrc(getString(o, "img"));
				m.setDirectorName(getString(o, "dN"));
				m.setActorName1(getString(o, "aN1"));
				m.setActorName2(getString(o, "aN2"));
				m.setShowdate(getLong(o, "rD"));
				list.add(m);
			}
		}

		return list;
	}

	public static List<Cinema> getNearByCinemas(String cx) throws JSONException {
		List<Cinema> list = new ArrayList<Cinema>();

		JSONObject j = new JSONObject(cx);
		if (!j.isNull("cs")) {
			JSONArray array = j.getJSONArray("cs");
			for (int i = 0; i < array.length(); i++) {
				JSONObject o = array.getJSONObject(i);

				Cinema c = new Cinema();
				c.setName(getString(o, "n"));
				c.setId(getInt(o, "id"));
				c.setGeoLatitude(getDouble(o, "addrN"));
				c.setGeoLongitude(getDouble(o, "addrE"));
				list.add(c);
			}
		}

		return list;
	}

	public static SignInResult getSignInResult(String cx) throws JSONException {
		SignInResult s = new SignInResult();

		JSONObject j = new JSONObject(cx);
		s.setSuccess(getBoolean(j, "success"));
		s.setError(getString(j, "error"));
		return s;
	}

	public static RatingResult getRatingMovie(String cx) throws JSONException {
		RatingResult s = new RatingResult();

		JSONObject j = new JSONObject(cx);

		s.setError(getString(j, "e"));

		if (!j.isNull("r")) {
			JSONObject o = j.getJSONObject("r");
			s.setRatingCount(getInt(o, "rC"));
			s.setRatingScore(getDouble(o, "r"));
		}
		return s;
	}

}

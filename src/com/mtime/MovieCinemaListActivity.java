package com.mtime;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;
import android.widget.ViewFlipper;
import android.widget.ExpandableListView.OnChildClickListener;

import com.mtime.data.Cinema;
import com.mtime.data.Movie;
import com.mtime.util.MtimeUtils;

public class MovieCinemaListActivity extends AbstractMtimeActivity {

    int movieId;
    Movie movie;
    String movieName, cinemaName;
    TextView navName;

    private ViewFlipper vflipper;

    private int date;
    Button bt_Today, bt_Tomorrow, bt_AfterTomorrow;

    final String[] days = MtimeUtils.getDays(3);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.act_movie_cinema_list);

        vflipper = (ViewFlipper) findViewById(R.id.flipper);
        navName = (TextView) findViewById(R.id.tv_movieName);

        movieId = getIntent().getExtras().getInt(Constants.KEY_MOVIE_ID);

        cinemaName = getIntent().getExtras().getString(
                Constants.KEY_CINEMA_NAME);
        movieName = getIntent().getExtras().getString(Constants.KEY_MOVIE_NAME);

        navName.setText(movieName);
        date = getIntent().getExtras().getInt(Constants.KEY_DATE);
        if (date == 0)
            date = Integer.parseInt(days[0]);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Fresh the mainbody
        mainBody = findViewById(R.id.cinema_list);
    }

    private void setUpCinemaListView(final int locationId, final int movieId,
            final int date) {

        bt_Today = (Button) findViewById(R.id.bt_Today);
        bt_Tomorrow = (Button) findViewById(R.id.bt_Tomorrow);
        bt_AfterTomorrow = (Button) findViewById(R.id.bt_AfterTomorrow);

        bt_Today.setText(MtimeUtils.getDayDesc(days[0]));
        bt_Tomorrow.setText(MtimeUtils.getDayDesc(days[1]));
        bt_AfterTomorrow.setText(MtimeUtils.getDayDesc(days[2]));

        OnClickListener lsner = new OnClickListener() {
            @Override
            public void onClick(View view) {
                int date = 0;
                switch (view.getId()) {
                case R.id.bt_Today:
                    date = Integer.parseInt(days[0]);
                    break;
                case R.id.bt_Tomorrow:
                    date = Integer.parseInt(days[1]);
                    break;
                case R.id.bt_AfterTomorrow:
                    date = Integer.parseInt(days[2]);
                    break;

                }
                setUpCinemaListView(locationId, movieId, date);
            }
        };

        bt_Today.setOnClickListener(lsner);
        bt_Tomorrow.setOnClickListener(lsner);
        bt_AfterTomorrow.setOnClickListener(lsner);

        refreshButtonBg(date);
        new Task() {
            LinkedHashMap<String, List<Cinema>> cinemaListMap;

            @Override
            public void after() {

                setUpCinemaListView(locationId, movieId, date, cinemaListMap);
            }

            @Override
            public void before() throws Exception {
                cinemaListMap = rs.getLocationMovieShowtimes(locationId,
                        movieId, date);
            }
        }.start();
    }

    private void setBtnSelected(Button btn) {
        btn.setBackgroundResource(R.drawable.btn_date_background_selected);
        btn.setClickable(false);
    }

    private void setBtnUnSelected(Button btn) {
        btn.setBackgroundResource(R.drawable.btn_date_background_default);
        btn.setClickable(true);
    }

    private void refreshButtonBg(int selectedDate) {

        if (selectedDate == Integer.parseInt(days[0])) {
            setBtnSelected(bt_Today);
            setBtnUnSelected(bt_Tomorrow);
            setBtnUnSelected(bt_AfterTomorrow);
        } else if (selectedDate == Integer.parseInt(days[1])) {
            setBtnUnSelected(bt_Today);
            setBtnSelected(bt_Tomorrow);
            setBtnUnSelected(bt_AfterTomorrow);
        } else if (selectedDate == Integer.parseInt(days[2])) {
            setBtnUnSelected(bt_Today);
            setBtnUnSelected(bt_Tomorrow);
            setBtnSelected(bt_AfterTomorrow);
        }

    }

    private void setUpCinemaListView(final int locationId, final int movieId,
            final int date,
            final LinkedHashMap<String, List<Cinema>> cinemaListMap) {

        Log.i(Constants.LOGTAG, "setUpCinemaListView...");

        ExpandableListView list = (ExpandableListView) findViewById(R.id.cinema_list);

        if (cinemaListMap.size() == 0) {
            emptyAlert.setVisibility(View.VISIBLE);
            return;
        } else {
            emptyAlert.setVisibility(View.GONE);
        }

        list.setAdapter(getCinemaListData(locationId, movieId, date,
                cinemaListMap));

        list.setOnChildClickListener(new OnChildClickListener() {

            public boolean onChildClick(ExpandableListView arg0, View arg1,
                    int groupPosition, int childPosition, long id) {

                Intent i = new Intent();
                i.setClass(MovieCinemaListActivity.this,
                        MovieSessionListActivity.class);

                i.putExtra(Constants.KEY_MOVIE_ID, movieId);

                String[] k = cinemaListMap.keySet().toArray(
                        new String[cinemaListMap.size()]);
                Cinema c = cinemaListMap.get(k[groupPosition]).get(
                        childPosition);
                i.putExtra(Constants.KEY_CINEMA_ID, c.getId());
                i.putExtra(Constants.KEY_CINEMA_NAME, c.getName());

                i.putExtra(Constants.KEY_DATE, date);
                i.putExtra(Constants.KEY_MOVIE_NAME, movieName);

                MovieCinemaListActivity.this.startActivity(i);
                return false;

            }

        });

        if (cinemaListMap.size() > 0)
            list.expandGroup(0);
    }

	private void showMovieDetailView(final int movieId) {
		new Thread() {
			@Override
			public void run() {
				movie = rs.getMovieDetail(movieId);
				Message message = movieDetailHandler.obtainMessage(0, this);
				movieDetailHandler.sendMessage(message);			}
		}.start();
	}
	
	final Handler movieDetailHandler = new Handler() {
		@Override
		public void handleMessage(Message message) {
			if (null == movie)
				MtimeUtils.gotoErrorActivity(MovieCinemaListActivity.this);
			else
				MovieDetailActivity.showMovieDetailView(
						MovieCinemaListActivity.this, movie, vflipper);
		}		
	};

    private static final String NAME = "NAME";
    private static final String REMAINS = "REMAINS";

    private ExpandableListAdapter getCinemaListData(int locationId,
            int movieId, int date,
            LinkedHashMap<String, List<Cinema>> cinemaListMap) {

        List<Map<String, String>> groupData = new ArrayList<Map<String, String>>();
        List<List<Map<String, String>>> childData = new ArrayList<List<Map<String, String>>>();
        for (String locationName : cinemaListMap.keySet()) {
            HashMap<String, String> gMap = new HashMap<String, String>();
            gMap.put(NAME, locationName);
            groupData.add(gMap);

            List<Map<String, String>> l = new ArrayList<Map<String, String>>();
            for (Cinema c : cinemaListMap.get(locationName)) {
                Map<String, String> cMap = new HashMap<String, String>();
                cMap.put(NAME, MtimeUtils.tinyString(c.getName(), 10));
                cMap.put("REMAINS", getString(R.string.movie_rest_times) + c.getRemainingShowtimeCount());
                l.add(cMap);
            }
            childData.add(l);
        }

        // Set up our adapter
        return new SimpleExpandableListAdapter(this, groupData,
                android.R.layout.simple_expandable_list_item_1,
                new String[] { NAME }, new int[] { android.R.id.text1,
                        android.R.id.text2 }, childData,
                R.layout.item_cinema_info, new String[] { NAME, REMAINS },
                new int[] { android.R.id.text1, android.R.id.text2 });
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        Dialog dialog = super.onCreateDialog(id);
        if (dialog != null)
            return dialog;

        switch (id) {

        case DIALOG_NO_MOVIE_IN_THIS_CINEMA: {
            return new AlertDialog.Builder(MovieCinemaListActivity.this)
                    .setTitle(R.string.no_movie_in_this_cinema).setIcon(
                            android.R.drawable.ic_dialog_info)
                    .setNeutralButton(R.string.check_other_cinema,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int whichButton) {
                                    Intent i = new Intent();
                                    i.setClass(MovieCinemaListActivity.this,
                                            MovieCinemaListActivity.class);

                                    i.putExtra(Constants.KEY_MOVIE_ID, movieId);
                                    // i.putExtra(Constants.KEY_CINEMA_NAME,
                                    // cinemaName);

                                    startActivity(i);
                                    finish();
                                }
                            }).create();

        }
        }
        return null;
    }

    @Override
    void doOperation() {

        setUpCinemaListView(currentLocation.getId(), movieId, date);
        showMovieDetailView(movieId);

    }
}